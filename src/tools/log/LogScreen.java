package tools.log;

/**
 * write logs on screen
 * 
 */
public class LogScreen implements Log {

	@Override
	public void writeLog(String s) {
		// print the log
		System.out.println(s);
	}

}
