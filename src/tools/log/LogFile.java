package tools.log;

import java.io.*;

/**
 * Write logs in a file
 * 
 */
public class LogFile implements Log {

	/**
	 * File where writing logs
	 */
	BufferedWriter bf;

	/**
	 * build log file
	 * 
	 * @param date date for filename
	 */
	public LogFile(String date) {
		// filelog creation
		try {
			bf = new BufferedWriter(new FileWriter(new File(date + ".log")));
		} catch (IOException e) {
			// problem when creating the logfile 
			e.printStackTrace();
			System.out.println("*** ERROR file of creation " + date + " ***");
			System.exit(1);
		}
	}

	@Override
	public void writeLog(String s) {
		try {
			bf.write(s + "\n");
			bf.flush();
		} catch (IOException e) {
			// writing problem
			e.printStackTrace();
			System.out.println("*** ERROR while writing in logfile ***");
			System.exit(1);
		}
	}

}
