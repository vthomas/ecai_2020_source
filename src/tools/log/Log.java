package tools.log;

/**
 * interface defining what is a Log
 */
public interface Log {

	/**
	 * method to write something in logfile
	 * 
	 * @param s string to write
	 */
	public void writeLog(String s);

}
