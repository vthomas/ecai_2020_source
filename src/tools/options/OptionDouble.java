package tools.options;

public class OptionDouble extends Option {

	/**
	 * current option value
	 */
	double value;

	public OptionDouble(String nom, double val, String descriptif) {
		super(nom, descriptif);
		this.value = val;
	}

	@Override
	public void setValue(String val) {
		this.value = Double.parseDouble(val);
	}

	@Override
	public double getValueDouble() {
		return this.value;
	}

	//////////// OPTION INFORMATION ///////////////////////

	@Override
	public String showValue() {
		return "" + this.value;
	}

	@Override
	public String getType() {
		return "double";
	}

	//////////// ACCESS ERROR ///////////////////////

	@Override
	public void setValue(int value) {
		throw new Error("Bad type : int for double option '" + this.nom + "'");
	}

	@Override
	public int getValueInt() {
		throw new Error("Bad type : int from double option '" + this.nom + "'");
	}

	@Override
	public String getValueString() {
		throw new Error("Bad type : string for double option '" + this.nom + "'");
	}

}
