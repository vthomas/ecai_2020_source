package tools.options;

/**
 * permet de representer une option (abstraite)
 * 
 * @author Vthomas
 *
 */
public abstract class Option {

	/**
	 * le nom de l'option
	 */
	String nom;

	/**
	 * le descriptif
	 */
	String descriptif;

	/**
	 * option cachee ou non
	 */
	boolean hidden;

	/**
	 * creer une option
	 */
	public Option(String nom, String descriptif) {
		this.nom = nom;
		this.descriptif = descriptif;
		this.hidden = false;
	}

	/**
	 * cacher une option
	 * 
	 * @param hidden true si on souhaite cacher option
	 */
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	/**
	 * give a value to option
	 * 
	 * @param value
	 */
	public abstract void setValue(int value);

	public abstract void setValue(String val);

	/**
	 * @return value depending on the option type
	 */
	public abstract int getValueInt();

	public abstract String getValueString();

	public abstract double getValueDouble();

	/**
	 * @return value as string
	 */
	public abstract String showValue();

	/**
	 * @return option type
	 */
	public abstract String getType();
}
