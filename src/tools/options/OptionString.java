package tools.options;

/**
 * a string option
 * 
 * @author vthomas
 *
 */
public class OptionString extends Option {

	String value;

	public OptionString(String nom, String value, String descriptif) {
		super(nom, descriptif);
		this.value = value;
	}

	@Override
	public void setValue(String val) {
		this.value = val;
	}

	@Override
	public String getValueString() {
		return this.value;
	}
	
	////////// OPTION INFORMATION ////////////

	@Override
	public String showValue() {
		return this.value;
	}
	
	@Override
	public String getType() {
		return "string";
	}

	////////////// ERROR FOR INT ACCESS //////////////

	@Override
	public int getValueInt() {
		throw new Error("Bad type : int from string option '" + this.nom + "'");
	}

	@Override
	public void setValue(int valeur) {
		throw new Error("Bad type : int for string option '" + this.nom + "'");

	}

	@Override
	public double getValueDouble() {
		throw new Error("Bad type : double for string option '" + this.nom + "'");
	}

	

}
