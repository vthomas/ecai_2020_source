package tools.options;

public class OptionEntier extends Option {

	/**
	 * la valeur de l'option
	 */
	int valeur;

	/**
	 * construit une option entiere
	 * 
	 * @param nom        nom de l'option
	 * @param entier     valeur de l'option
	 * @param descriptif descriptif de l'option
	 */
	public OptionEntier(String nom, int entier, String descriptif) {
		super(nom, descriptif);
		this.valeur = entier;
	}

	/**
	 * change la valeur de l'option
	 */
	public void setValue(String valeur) {
		this.valeur = Integer.parseInt(valeur);
	}

	/**
	 * change la valeur de l'option
	 */
	@Override
	public void setValue(int valeur) {
		this.valeur = valeur;
	}

	/**
	 * retourne la valeur de l'option
	 */
	@Override
	public int getValueInt() {
		return this.valeur;
	}

	@Override
	public String showValue() {
		return "" + this.valeur;
	}

	//////////////// ERROR WITH INTEGER OPTION /////////////////////////

	@Override
	public String getValueString() {
		throw new Error("bad type");
	}

	@Override
	public double getValueDouble() {
		throw new Error("Bad type : double from int option '" + this.nom + "'");
	}

	@Override
	public String getType() {
		return "int";
	}

}
