package model.pomdp;

/**
 * builds a simulator from a POMDP to execute action
 * 
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public class Simulator<S, A, O> {

	/**
	 * true underlying model
	 */
	private POMDP<S, A, O> pomdp;

	/**
	 * current belief
	 */
	private Belief<S> belief;

	/**
	 * initial belief
	 */
	private Belief<S> belief0;

	/**
	 * current true state
	 */
	private S state;

	/**
	 * builds a simulator
	 * 
	 * @param pb pomdp problem
	 * @param b  initial belief b0
	 */
	public Simulator(POMDP<S, A, O> pb, Belief<S> b) {
		this.setPomdp(pb);
		this.setBelief(b);
		this.belief0 = b;

		// sample true state from b0
		this.setState(b.sample());
	}

	/**
	 * executes an action by simulating the process and updates information :
	 * <ul>
	 * <li>true system state
	 * <li>corresponding belief  
	 * </ul>
	 * and returns received observation
	 * 
	 * @param action performed action 
	 * @return received observation 
	 */
	public O execute(A action) {
		// samples arrival state and update current state
		S arrivalState = this.getPomdp().transition(this.getState(), action).sample();
		double rec = this.getPomdp().r(this.getState(), action);
		this.setState(arrivalState);

		// samples observation
		O obs = this.getPomdp().observe(action, arrivalState).sample();

		// bayes update of current belief
		BeliefUpdate<S, A, O> bu = new BeliefUpdate<>(this.getPomdp());
		Belief<S> bPrime = bu.updateBelief(this.getBelief(), action, obs);
		this.setBelief(bPrime);

		// return received observation
		return obs;
	}

	public POMDP<S, A, O> getPomdp() {
		return pomdp;
	}

	public void setPomdp(POMDP<S, A, O> pomdp) {
		this.pomdp = pomdp;
	}

	public S getState() {
		return state;
	}

	public void setState(S etat) {
		this.state = etat;
	}

	public Belief<S> getBelief() {
		return belief;
	}

	public void setBelief(Belief<S> belief) {
		this.belief = belief;
	}

	/**
	 * reinitializes simulation with initial belief
	 */
	public void initialiser() {
		this.belief = belief0;
		this.state = this.belief.sample();
	}

}
