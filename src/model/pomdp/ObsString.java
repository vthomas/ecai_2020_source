package model.pomdp;

/**
 * class to be used to define observation (include equals and hshcode on string)
 * 
 * @author vthomas
 *
 */
public class ObsString {

	/**
	 * observation name
	 */
	String obsName;

	public ObsString(String s) {
		this.obsName = s;
	}

	public int hashCode() {
		return this.obsName.hashCode();
	}

	public boolean equals(Object o) {
		return this.obsName.equals(((ObsString) o).obsName);
	}

	public String toString() {
		return this.obsName;
	}

}
