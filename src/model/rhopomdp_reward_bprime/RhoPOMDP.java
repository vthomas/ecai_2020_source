package model.rhopomdp_reward_bprime;

import java.util.List;

import model.mdp.Distribution;
import model.pomdp.Belief;

public interface RhoPOMDP<S, A, O> {

	/**
	 * reward function
	 * 
	 * @param startB starting belief
	 * @param action performed action
	 * @param arrivB arrival belief
	 * 
	 * @return reward associated to transition
	 */
	public abstract double reward(Belief<S> startB, A action, Belief<S> arrivB);

	/**
	 * transition function => distribution on arrival state
	 * 
	 * @param initS  initial state
	 * @param action performed action
	 * @return distribution on arrival state
	 */
	public abstract Distribution<S> transition(S initS, A action);

	/**
	 * @return set of possible states  of rho-POMDP
	 */
	public abstract List<S> allStates();

	/**
	 * @return set of possible actions  of rho-POMDP
	 */
	public abstract List<A> allAction();

	/**
	 * observation : probability distribution on observation o
	 * 
	 * @param initS  initial state
	 * @param action performed action
	 * @param arrivS arrival state
	 * @return distribution on observation
	 */
	public abstract Distribution<O> observation(S starting, A action, S arrivS);

	/**
	 * @return set of possible observations
	 */
	public abstract List<O> allObs();

	/**
	 * @return gamma value
	 */
	public abstract double getGamma();

	/**
	 * @return initialBelief
	 */
	public abstract Belief<S> getB0();

}
