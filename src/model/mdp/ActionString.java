package model.mdp;

/**
 * represents action by a string
 */
public class ActionString {

	/**
	 * name of action
	 */
	String action;

	/**
	 * creates an action
	 * 
	 * @param s action name
	 */
	public ActionString(String s) {
		this.action = s;
	}

	/**
	 * @return action name
	 */
	public String getAction() {
		return action;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActionString other = (ActionString) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		return true;
	}

	public String toString() {
		return this.action;
	}

}
