package model.mdp;

/**
 * generic state on a 2D discrete space
 */
public class StateXY extends StateAbstract {

	/**
	 * coordinates
	 */
	public int x;
	public int y;

	/**
	 * creates a new state
	 * 
	 * @param x coordinate
	 * @param y coordinate
	 */
	public StateXY(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	/**
	 * creates a copy of a statexy
	 * 
	 * @param sxy state to clone
	 */
	public StateXY(StateXY sxy) {
		this.x = sxy.x;
		this.y = sxy.y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StateXY other = (StateXY) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	public String toString() {
		return ("(" + x + "," + y + ")");
	}

	/**
	 * equality with other coordinates
	 * 
	 * @param i x value
	 * @param j y value
	 * @return true if coordinates are the same
	 */
	public boolean equals(int i, int j) {
		return (this.x == i) && (this.y == j);
	}

}
