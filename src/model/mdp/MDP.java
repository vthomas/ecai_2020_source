package model.mdp;

import java.util.List;

/**
 * MDP model
 * 
 * @param <S> state space
 * @param <A> action space
 */
public interface MDP<S, A> {

	/**
	 * reward function
	 * 
	 * @param dep    starting state
	 * @param action performed action
	 * @param res    arrival state
	 * @return reward associated to the transition
	 */
	public abstract double reward(S dep, A action, S res);

	/**
	 * distribution on arrival states
	 * 
	 * @param s      starting state
	 * @param action performed action
	 * @return distribution on arrival states
	 */
	public abstract Distribution<S> transition(S s, A action);

	/**
	 * @return set of possible states
	 */
	public abstract List<S> allStates();

	/**
	 * @return set of possible actions
	 */
	public abstract List<A> allAction();

}
