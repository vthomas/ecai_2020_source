package problem.rhopomdp_belief_bprime.navigation;

public class NavMap {

	/**
	 * possible walls
	 */
	boolean[][] walls;

	/**
	 * reward when reaching cell
	 */
	double[][] reward;

	/**
	 * goal
	 */
	int gX, gY;

	/**
	 * Map for navigation problem
	 */
	public NavMap(boolean[][] pWalls, double[][] pReward, int gX, int gY) {
		this.walls = pWalls;
		this.gX = gX;
		this.gY = gY;
		this.reward = pReward;
	}

	public boolean isWall(int i, int j) {
		return this.walls[i][j];
	}

	public boolean isGoal(int x, int y) {
		return (this.gX == x) && (this.gY == y);
	}
	
	public double getReward(int x, int y) {
		return reward[x][y];
	}

}
