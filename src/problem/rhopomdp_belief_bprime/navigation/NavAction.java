package problem.rhopomdp_belief_bprime.navigation;

import java.util.ArrayList;
import java.util.List;

import model.mdp.ActionString;

/**
 * action for navigation
 */
public class NavAction extends ActionString {

	/**
	 * static actions
	 */
	public static final NavAction FORWARD = new NavAction("forward");
	public static final NavAction LEFT = new NavAction("turn-left");
	public static final NavAction RIGHT = new NavAction("turn-right");
	public static final NavAction AROUND = new NavAction("turn-around");
	public static final NavAction NOOP = new NavAction("stay-in-place");

	/**
	 * all actions
	 */
	public static List<NavAction> actions = createActions();

	/**
	 * navigation action are just string
	 * 
	 * @param s name of actin
	 */
	private NavAction(String s) {
		super(s);
	}

	/**
	 * 
	 * @return possible actions as a list
	 */
	private static List<NavAction> createActions() {
		List<NavAction> act = new ArrayList<NavAction>();
		act.add(NOOP);
		act.add(FORWARD);
		act.add(LEFT);
		act.add(RIGHT);
		act.add(AROUND);
		return act;
	}

}
