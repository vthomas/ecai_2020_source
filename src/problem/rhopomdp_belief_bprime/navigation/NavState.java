package problem.rhopomdp_belief_bprime.navigation;

import java.util.HashMap;
import java.util.Map;
import model.mdp.StateAbstract;

/**
 * navigation state
 */
public class NavState extends StateAbstract {

	/**
	 * existing states to prevent duplication
	 */
	static Map<NavState, NavState> states = new HashMap<>();
	static boolean preventDuplication = true;

	/**
	 * possible orientations
	 */
	public final static int NORTH = 0;
	public final static int EAST = 1;
	public final static int SOUTH = 2;
	public final static int WEST = 3;

	/**
	 * final state
	 */
	final static NavState FINAL = new NavState(-1, -1, -1);

	/**
	 * coordinates and orientation
	 */
	int x, y, or;

	/**
	 * builder of states to prevent duplication
	 * 
	 * @param x      coordinate x axis
	 * @param y      coordinate y axis
	 * @param facing orientation
	 */
	public static NavState getState(int x, int y, int facing) {
		NavState temp = new NavState(x, y, facing);

		// if duplication is not prevented, each state is a new one
		if (!preventDuplication)
			return temp;

		// if duplication is prevented, use existing states if exists
		if (states.containsKey(temp))
			return states.get(temp);

		// if state does not exist, add it to existing states
		states.put(temp, temp);
		return temp;
	}

	/**
	 * create a state for navigation issue
	 * 
	 * @param x      x coord
	 * @param y      y coord
	 * @param facing orientation
	 */
	private NavState(int px, int py, int facing) {
		this.x = px;
		this.y = py;
		this.or = facing;
	}

	/**
	 * return next pose after a forward move
	 * 
	 * @return new navstate after forward move
	 */
	public NavState getForward() {
		// new values after forward
		int nx = this.x;
		int ny = this.y;

		// depend on orientation
		switch (this.or) {
		case NORTH:
			ny = ny - 1;
			break;
		case SOUTH:
			ny = ny + 1;
			break;
		case EAST:
			nx = nx + 1;
			break;
		case WEST:
			nx = nx - 1;
			break;
		default:
			throw new Error("orientation unknown");
		}
		return getState(nx, ny, this.or);
	}

	/**
	 * return next pose after backward
	 * 
	 * @return new navstate
	 */
	public NavState getBackward() {
		// change orientation
		NavState nav = getTurn();
		// return forward
		return nav.getForward();
	}

	/**
	 * turn around
	 * 
	 * @return
	 */
	public NavState getTurn() {
		NavState nav = new NavState(this.x, this.y, (this.or + 2) % 4);
		return nav;
	}

	/**
	 * return next pose after turning left
	 * 
	 * @return new navstate
	 */
	public NavState getLeft() {
		return getState(this.x, this.y, (this.or + 3) % 4);
	}

	/**
	 * return next pose after turning right
	 * 
	 * @return new navstate
	 */
	public NavState getRight() {
		return getState(this.x, this.y, (this.or + 1) % 4);
	}

	/************************************
	 ************************************
	 * hashcode and equals
	 ************************************
	 ************************************
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + or;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NavState other = (NavState) obj;
		if (or != other.or)
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	/**
	 * @return position variation depending on orientation
	 */
	public int[] getDeltaOrientation() {
		int[] res = new int[2];

		switch (this.or) {
		case NORTH:
			res[1] = -1;
			break;
		case SOUTH:
			res[1] = 1;
			break;
		case EAST:
			res[0] = 1;
			break;
		case WEST:
			res[0] = -1;
			break;
		default:
			throw new Error("orientation unknown");
		}

		return res;
	}

	public String toString() {
		return "(" + this.x + "," + this.y + "," + this.or + ")";
	}

}
