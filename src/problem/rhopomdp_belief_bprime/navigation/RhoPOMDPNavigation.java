package problem.rhopomdp_belief_bprime.navigation;

import java.util.List;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * generic navigation problem from Littman95 appendix "Learning policies for
 * partially observable environments" tech report
 *
 */
public abstract class RhoPOMDPNavigation implements RhoPOMDP<NavState, NavAction, NavObs> {

	/**
	 * positive reward
	 */
	public double POSITIVE = 10.;

	/**
	 * navigation map for this problem
	 */
	private NavMap navmap;
	
	/**
	 * state POMDP reward
	 * 
	 * @param arrivS arrival state
	 * @return reward for reaching this state
	 */
	public double rewardState(NavState arrivS) {
		if (arrivS == NavState.FINAL)
			return 0;

		if (navmap.isGoal(arrivS.x, arrivS.y))
			return POSITIVE;

		// return reward from arrivalstate
		return navmap.getReward(arrivS.x, arrivS.y);

	}

	@Override
	/**
	 * return reward based on linear combination from arrivB
	 */
	public double reward(Belief<NavState> startB, NavAction action, Belief<NavState> arrivB) {
		double rec = 0;

		// return linear combination of arrivalB
		for (NavState s : arrivB.keySet()) {
			// += reward s' * b(s')
			rec = rec + arrivB.get(s) * rewardState(s);
		}

		return rec;
	}

	public abstract Belief<NavState> getB0();

	@Override
	public Distribution<NavState> transition(NavState s, NavAction action) {
		// if starting state = objective or final
		if ((s == NavState.FINAL) || (this.navmap.isGoal(s.x, s.y))) {
			// go to final
			Distribution<NavState> dis = new Distribution<>();
			dis.addProbability(NavState.FINAL, 1.);
			return dis;
		}

		// if action = turn left
		if (action == NavAction.LEFT) {
			Distribution<NavState> dis = new Distribution<>();
			// stay same orientation
			dis.addProbability(s, 0.1);
			NavState left = s.getLeft();
			dis.addProbability(left, 0.7);
			// other orientation
			NavState other = left.getLeft();
			dis.addProbability(other, 0.1);
			other = other.getLeft();
			dis.addProbability(other, 0.1);
			return dis;
		}

		// if action = turn right
		if (action == NavAction.RIGHT) {
			Distribution<NavState> dis = new Distribution<>();
			// stay same orientation
			dis.addProbability(s, 0.1);
			NavState right = s.getRight();
			dis.addProbability(right, 0.7);
			// other orientation
			NavState other = right.getRight();
			dis.addProbability(other, 0.1);
			other = other.getRight();
			dis.addProbability(other, 0.1);
			return dis;
		}

		// if action = turn around
		if (action == NavAction.AROUND) {
			Distribution<NavState> dis = new Distribution<>();
			// stay same orientation
			dis.addProbability(s, 0.1);
			// one turn
			NavState right = s.getRight();
			dis.addProbability(right, 0.15);
			// two turns
			NavState other = right.getRight();
			dis.addProbability(other, 0.6);
			// three turns
			other = other.getRight();
			dis.addProbability(other, 0.15);
			return dis;
		}

		// if action = noop
		if (action == NavAction.NOOP) {
			Distribution<NavState> dis = new Distribution<>();
			dis.addProbability(s, 1.);
			return dis;
		}

		// if action = forward
		if (action == NavAction.FORWARD) {
			Distribution<NavState> dis = new Distribution<>();

			// stay
			dis.addProbability(s, 0.05);

			// forward if possible (else same place)
			{
				NavState forward = s.getForward();
				if (navmap.isWall(forward.x, forward.y))
					forward = s;
				dis.addProbability(forward, 0.8);
			}

			// left + forward
			{
				NavState left = s.getLeft().getForward();
				if (navmap.isWall(left.x, left.y))
					left = s;
				dis.addProbability(left, 0.05);
			}

			// right + forward
			{
				NavState right = s.getRight().getForward();
				if (navmap.isWall(right.x, right.y))
					right = s;
				dis.addProbability(right, 0.05);
			}

			// backwards
			{
				NavState back = s.getTurn().getForward();
				NavState backSameOr = back.getTurn();

				if (navmap.isWall(back.x, back.y)) {
					back = s;
					backSameOr = s;
				}
				dis.addProbability(back, 0.025);
				dis.addProbability(backSameOr, 0.025);
			}
			return dis;
		}

		// TODO Auto-generated method stub
		throw new Error("unknown action: '"+action+"'");
	}

	@Override
	public Distribution<NavObs> observation(NavState starting, NavAction action, NavState arrivS) {
		// distribution obs
		Distribution<NavObs> distObs = new Distribution<>();

		// get true observation
		NavObs trueObs = NavObs.getTrueObs(arrivS, navmap);

		// if trueObs isgoal
		if (trueObs.getVal(NavObs.GOAL)) {
			distObs.addProbability(trueObs, 1.0);
			return distObs;
		}

		// basic probabilities
		double pOkWall = 0.9;
		double pNotOkWall = 0.1;
		double pOkEmpty = 0.95;
		double pNotOkEmpty = 0.05;

		// build probability for all observations
		for (int t = 0; t < 16; t++) {
			double pb = 1;
			boolean[] boolObs = new boolean[5];

			// for each obs part
			int o = 1; // used for accessing corresponding digit
			int num = 0;
			while (o <= Math.pow(2, 3) + 0.5) {
				boolean d = (((t & o) == 0));
				boolObs[num] = d;
				// if same
				if (d == trueObs.getVal(num)) {
					// true value
					if (trueObs.getVal(num))
						// when wall
						pb = pb * pOkWall;
					else
						// when empty
						pb = pb * pOkEmpty;
				} else {
					// mistaken
					if (trueObs.getVal(num))
						// when wall
						pb = pb * pNotOkWall;
					else
						// when empty
						pb = pb * pNotOkEmpty;
				}
				// next part
				num = num + 1;
				o = o * 2;
			}
			// add observation with pb
			distObs.addProbability(NavObs.getObs(boolObs), pb);
		}

		return (distObs);
	}

	/*******************************
	 * List and gamma
	 ********************************/

	@Override
	public List<NavState> allStates() {
		// TODO Auto-generated method stub
		throw new Error("TODO");
	}

	@Override
	public List<NavAction> allAction() {
		return NavAction.actions;
	}

	@Override
	public List<NavObs> allObs() {
		return NavObs.allObs;
	}

	@Override
	public double getGamma() {
		// TODO Auto-generated method stub
		return 0.95;
	}

	public NavMap getNavmap() {
		return navmap;
	}

	public void setNavmap(NavMap navmap) {
		this.navmap = navmap;
	}

}
