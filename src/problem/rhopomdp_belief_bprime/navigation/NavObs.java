package problem.rhopomdp_belief_bprime.navigation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NavObs {

	/**
	 * possible directions
	 */
	public static int FRONT = 0;
	public static int RIGHT = 1;
	public static int BACK = 2;
	public static int LEFT = 3;
	public static int GOAL = 4;

	/**
	 * existing observations to prevent duplication
	 */
	static Map<NavObs, NavObs> existingObs = new HashMap<>();

	/**
	 * all observations
	 */
	public static List<NavObs> allObs = getObs();

	/**
	 * presence of walls on each direction + isGoal ?
	 */
	private boolean[] values;

	/**
	 * getter to prevent duplication
	 * 
	 * @param state : pose of robot
	 * @param map   : map of maze
	 */
	public static NavObs getTrueObs(NavState state, NavMap map) {

		// if FINAL
		if (state == NavState.FINAL) {
			boolean[] obs = new boolean[5];
			obs[GOAL] = true;
			// build observation
			NavObs navObs = new NavObs(obs);

			// get from map
			if (existingObs.containsKey(navObs))
				return existingObs.get(navObs);

			// if not exists, add it
			existingObs.put(navObs, navObs);
			return navObs;
		}

		// compute correct booleans
		boolean[] obs = new boolean[5];

		// get orientation
		int[] delta = state.getDeltaOrientation();

		// all orientation
		int x = state.x;
		int y = state.y;

		obs[GOAL] = map.isGoal(x, y);

		// if not the goal, estimate walls
		if (!obs[GOAL]) {
			obs[FRONT] = map.isWall(x + delta[0], y + delta[1]);
			obs[RIGHT] = map.isWall(x + delta[1], y - delta[0]);
			obs[BACK] = map.isWall(x - delta[0], y - delta[1]);
			obs[LEFT] = map.isWall(x - delta[1], y + delta[0]);
		}

		// build observation
		NavObs navObs = new NavObs(obs);

		// get from map
		if (existingObs.containsKey(navObs))
			return existingObs.get(navObs);

		// if not exists, add it
		existingObs.put(navObs, navObs);
		return navObs;
	}

	/**
	 * build an observation and prevent duplication
	 * 
	 * @param boolObs
	 * @return
	 */
	public static NavObs getObs(boolean[] boolObs) {
		NavObs navObs = new NavObs(boolObs);
		// get from map
		if (existingObs.containsKey(navObs))
			return existingObs.get(navObs);

		// if not exists, add it
		existingObs.put(navObs, navObs);
		return navObs;
	}

	/**
	 * private constructor to prevent duplication
	 * 
	 * @param tab observation values
	 */
	private NavObs(boolean[] tab) {
		values = tab;
	}

	/**
	 * build all possible observations
	 * 
	 * @return list of possible observation
	 */
	private static List<NavObs> getObs() {

		// observations for walls
		ArrayList<NavObs> listObs = new ArrayList<>();
		for (int i = 0; i < Math.pow(2, 4); i++) {
			boolean[] ob = { (i & 1) == 1, (i & 2) == 2, (i & 4) == 4, (i & 8) == 8, false };
			NavObs navObs = new NavObs(ob);

			// add this observation
			listObs.add(navObs);
			existingObs.put(navObs, navObs);
		}

		// goal observation
		boolean[] ob = { false, false, false, false, true };
		NavObs g = new NavObs(ob);
		listObs.add(g);
		existingObs.put(g, g);

		return listObs;
	}

	public boolean getVal(int index) {
		return this.values[index];
	}
	
	public String toString()
	{
		String res="";
		for (Boolean b:this.values)
			if (b)
				res=res+"X";
			else
				res=res+".";
		return res;
	}


	
	
	/************************************************************
	 * hashcode + equals
	 *************************************************************/

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(values);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NavObs other = (NavObs) obj;
		if (!Arrays.equals(values, other.values))
			return false;
		return true;
	}
	
	
}
