package problem.rhopomdp_belief_bprime.cameraClean;

/**
 * state representation for cameraclean problem
 *
 */
public class StateCamera {

	/**
	 * zone observed by the camera
	 */
	int cameraZone;

	/**
	 * zone of the object
	 */
	int objectZone;

	/**
	 * state of lens
	 */
	boolean lensClean;

	/**
	 * create a state for cameraclean problem
	 * 
	 * @param cam       zone observed by camera
	 * @param obj       zone of the trackef object
	 * @param cleanLens cleanness of lens
	 * 
	 */
	public StateCamera(int cam, int obj, boolean cleanLens) {
		this.cameraZone = cam;
		this.objectZone = obj;
		this.lensClean = cleanLens;
	}

	/**
	 * duplicate camera state
	 * 
	 * @param s state to duplicate
	 */
	public StateCamera(StateCamera s) {
		this.cameraZone = s.cameraZone;
		this.objectZone = s.objectZone;
		this.lensClean = s.lensClean;
	}

	/**
	 * 
	 * @return true if camera and object on same zone
	 */
	public boolean cameraOnObject() {
		return this.cameraZone == this.objectZone;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cameraZone;
		result = prime * result + (lensClean ? 1231 : 1237);
		result = prime * result + objectZone;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StateCamera other = (StateCamera) obj;
		if (cameraZone != other.cameraZone)
			return false;
		if (lensClean != other.lensClean)
			return false;
		if (objectZone != other.objectZone)
			return false;
		return true;
	}

	/**
	 * advance cameraPosition
	 * 
	 * @param nbZones number of zones
	 */
	public void advanceCamera(int nbZones) {
		this.cameraZone = (this.cameraZone + 1) % nbZones;
	}

	public String toString() {
		int clean = 0;
		if (this.lensClean)
			clean = 1;
		return "c" + this.cameraZone + "o" + this.objectZone + "e" + clean;
	}

}
