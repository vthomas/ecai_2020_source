package problem.rhopomdp_belief_bprime.cameraClean;

import model.pomdp.ObsString;

/**
 * Observations for Camera clean problem
 *
 */
public class ObsCamera extends ObsString {

	/**
	 * constant to define possible observations
	 */
	public static final ObsCamera TRUE = new ObsCamera("true");
	public static final ObsCamera FALSE = new ObsCamera("false");
	public static final ObsCamera NOPHOTO = new ObsCamera("nophotos");
	public static final ObsCamera[] ALL_OBS = { TRUE, FALSE, NOPHOTO };

	/**
	 * create observation (private - only use constant action)
	 */
	private ObsCamera(String s) {
		super(s);
	}

}
