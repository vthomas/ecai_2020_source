package problem.rhopomdp_belief_bprime.activeLoc;

import model.mdp.Distribution;
import model.pomdp.Belief;
import problem.rhopomdp_belief_bprime.mazeLocalization.ActionMaze;
import problem.rhopomdp_belief_bprime.mazeLocalization.ObsMaze;
import problem.rhopomdp_belief_bprime.mazeLocalization.RhoPOMDPMaze_PreComputed;
import problem.rhopomdp_belief_bprime.mazeLocalization.RhoPOMDPMaze_withoutPrecomputatioon;
import problem.rhopomdp_belief_bprime.mazeLocalization.StateMaze;

/**
 * active localisation
 * <li>it is close to mazealocalization
 * <li>except observation is given only when the agent "REST"
 * 
 *
 */
public class RhoPOMDPActiveLoc extends RhoPOMDPMaze_PreComputed {

	private static final ObsMaze OBS_0 = new ObsMaze(0);

	/**
	 * constructor
	 * 
	 * @param tab maze
	 */
	public RhoPOMDPActiveLoc(int[][] tab) {
		super(tab);
	}

	@Override
	/**
	 * change on observation process
	 * 
	 * @param start
	 * @param action
	 * @param end
	 * @return
	 */
	public Distribution<ObsMaze> observation(StateMaze start, ActionMaze action, StateMaze end) {
		// if action is rest
		if (action == ActionMaze.REST) {
			// returns right color
			return super.observation(start, action, end);
		} else {
			// no informative observation
			Distribution<ObsMaze> obsDist = new Distribution<>();
			obsDist.addProbability(OBS_0, 1.);
			return obsDist;
		}
	}

	@Override
	public Belief<StateMaze> getB0() {
		Belief<StateMaze> b0=new Belief<>();
		for (int i=0;i<this.getMaze().length;i++)
			for (int j=0;j<this.getMaze()[0].length;j++)
			{
				b0.put(new StateMaze(i, j), 1.);
			}
		b0.setUniform();
		return b0;
	}

}
