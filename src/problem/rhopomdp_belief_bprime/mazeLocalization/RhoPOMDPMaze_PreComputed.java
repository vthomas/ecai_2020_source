package problem.rhopomdp_belief_bprime.mazeLocalization;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * rhoPOMDP Maze problem
 * 
 * <ul>
 * 
 * <li>toric maze described by color on each location
 * 
 * <li>when agent is moving, it may slip and its move is replaced by an
 * orthogonal move (P_TRANSITION_SLIPPING).
 * 
 * <li>after the agent has moved, it receives an observation of the color of its
 * location (possibly with noise : P_OBS_ERROR)
 * 
 * </ul>
 * 
 */

public class RhoPOMDPMaze_PreComputed implements RhoPOMDP<StateMaze, ActionMaze, ObsMaze> {

	/**
	 * slipping probability
	 */
	protected double pTransitionSlipping = 0.;

	/**
	 * incorrect observation probability
	 */
	double pObsError = 0.;

	/**
	 * colors of maze locations
	 */
	private int[][] maze;

	/**
	 * colors number
	 */
	int numColors;

	/**
	 * initial belief
	 */
	Belief<StateMaze> belief0 = null;

	/**
	 * states and actions list
	 */
	List<StateMaze> states;
	protected List<ActionMaze> actions;
	List<ObsMaze> obs;

	/**
	 * static transition table
	 */
	private Map<StateMaze, Map<ActionMaze, Distribution<StateMaze>>> transitionTable;

	private Map<StateMaze, Map<ActionMaze, Map<StateMaze, Distribution<ObsMaze>>>> obsMatrix;

	/**
	 * deterministic rhopomdp maze creation.
	 * <ul>
	 * <li>proba of slipping = 0
	 * <li>proba of incorrect obs = 0
	 * </ul>
	 * 
	 * @param tab array defining the maze
	 */
	public RhoPOMDPMaze_PreComputed(int[][] tab) {
		// generate maze date
		this.setLabyrinthe(tab);

		// determine different colors number
		int maxColor = numColors();
		// number of colors = max + 1 (between 0 and numColors-1)
		this.numColors = maxColor + 1;

		// list of states
		int tx = getMaze().length;
		int ty = getMaze()[0].length;
		StateMaze[] allStates = StateMaze.allStates(tx, ty);
		states = Arrays.asList(allStates);

		// actions list
		actions = Arrays.asList(ActionMaze.ACTIONS);

		// observations list
		obs = Arrays.asList(ObsMaze.getAll(this.numColors));

		// generates transition table
		this.transitionTable = createTransitions();
		this.obsMatrix = createObservation();
	}

	/**
	 * create rhoPOMDP with slipping probability and possibility incorrect
	 * observations.
	 * 
	 * @param tab      maze description
	 * @param slipping slipping probability during transition
	 * @param obsError incorrect observation probability
	 */
	public RhoPOMDPMaze_PreComputed(int[][] tab, double slipping, double obsError) {
		this(tab);

		// if probabilities badly given
		if ((obsError > 1) || (obsError < 0) || (slipping > 1) || (slipping < 0))
			throw new Error("problem with probabilities");

		// update probabilities
		this.pObsError = obsError;
		this.pTransitionSlipping = slipping;
	}

	/**
	 * look for the number of colors of the maze
	 * 
	 * @return max color in maze
	 */
	private int numColors() {
		int tx = getMaze().length;
		int ty = getMaze()[0].length;
		int colorMax = 0;
		for (int i = 0; i < tx; i++)
			for (int j = 0; j < ty; j++) {
				// no negative index (to prevent issues)
				if (getMaze()[i][j] < 0) {
					getMaze()[i][j] = -getMaze()[i][j];
					throw new Error("Error : some colors are negatives");
				}

				// if higher than current max
				if (getMaze()[i][j] > colorMax)
					colorMax = getMaze()[i][j];
			}
		// number = coulmax + 1
		return colorMax;
	}

	/************ TRANSITION ********************/

	@Override
	public Distribution<StateMaze> transition(StateMaze s, ActionMaze action) {
		return this.transitionTable.get(s).get(action);
	}

	public Map<StateMaze, Map<ActionMaze, Distribution<StateMaze>>> createTransitions() {
		Map<StateMaze, Map<ActionMaze, Distribution<StateMaze>>> table = new HashMap<>();
		// for each state
		for (StateMaze state : this.allStates()) {
			Map<ActionMaze, Distribution<StateMaze>> tAction;
			tAction = new HashMap<>();
			// for each action
			for (ActionMaze action : this.allAction()) {
				// get distribution
				tAction.put(action, this.computeTransition(state, action));
			}
			table.put(state, tAction);
		}
		return table;
	}

	/**
	 * transition probability function. Slipping probability defines noise during
	 * transition.
	 * 
	 * <ul>
	 * <li>desired move is performed with probability (1-ERROR)
	 * <li>desired move is replaced by an orthogonal move with probability (ERROR/
	 * 2) for each direction
	 * </ul>
	 */
	public Distribution<StateMaze> computeTransition(StateMaze s, ActionMaze action) {
		// TODO optimization - possible to compute all in a static way (to prevent
		// computer time cost)

		// transition with slipping probability
		Distribution<StateMaze> dist = new Distribution<>();

		// verify action is a move or a rest
		if ((action != ActionMaze.EAST) && (action != ActionMaze.WEST) && (action != ActionMaze.NORTH)
				&& (action != ActionMaze.SOUTH) && (action != ActionMaze.REST)) {
			throw new Error("action non connue");
		}

		double probaErreur = pTransitionSlipping;
		// compute predicted state (toroidal world + action)
		StateMaze resultState = this.nextState(s, action);
		dist.addProbability(resultState, 1 - probaErreur);

		// if slipping probability exists
		if (pTransitionSlipping > 0) {
			// slipping in perpendicular directions
			ActionMaze ortho1 = action.ortho1();
			StateMaze res1 = this.nextState(s, ortho1);
			dist.addProbability(res1, probaErreur / 2);

			ActionMaze ortho2 = action.ortho2();
			StateMaze res2 = this.nextState(s, ortho2);
			dist.addProbability(res2, probaErreur / 2);
		}
		return dist;
	}

	/**
	 * compute next state from state and action
	 * 
	 * @param s      starting state
	 * @param action performed action
	 * @return next state
	 */
	private StateMaze nextState(StateMaze s, ActionMaze action) {

		StateMaze nextS = new StateMaze(s.x + action.dX, s.y + action.dY);

		// toroidal world
		// move x
		int tx = this.getMaze().length;
		if (nextS.x >= tx) {
			nextS.x -= tx;
		}
		if (nextS.x < 0) {
			nextS.x += tx;
		}

		// move y
		int ty = this.getMaze()[0].length;
		if (nextS.y >= ty) {
			nextS.y -= ty;
		}
		if (nextS.y < 0) {
			nextS.y += ty;
		}

		return nextS;
	}

	/************ REWARD ********************/

	@Override
	/**
	 * reward equals the negentropy difference
	 */
	public double reward(Belief<StateMaze> bStart, ActionMaze action, Belief<StateMaze> bEnd) {
		double startNegentropy = -bStart.getEntropy();
		double endNegentropy = -bEnd.getEntropy();
		return endNegentropy - startNegentropy;
	}

	/************ OBSERVATION ********************/

	@Override
	public Distribution<ObsMaze> observation(StateMaze start, ActionMaze action, StateMaze end) {
		return this.obsMatrix.get(start).get(action).get(end);
	}

	private Map<StateMaze, Map<ActionMaze, Map<StateMaze, Distribution<ObsMaze>>>> createObservation() {
		Map<StateMaze, Map<ActionMaze, Map<StateMaze, Distribution<ObsMaze>>>> obsCreated = new HashMap<>();
		// for each starting state
		for (StateMaze s : this.allStates()) {
			// for each action
			Map<ActionMaze, Map<StateMaze, Distribution<ObsMaze>>> distS = new HashMap<>();
			for (ActionMaze a : this.allAction()) {
				// for each arrival state
				Map<StateMaze, Distribution<ObsMaze>> distA = new HashMap<>();
				for (StateMaze sP : this.allStates()) {
					// get distribution
					Distribution<ObsMaze> distObs = this.computeObservation(s, a, sP);
					distA.put(sP, distObs);
				}
				distS.put(a, distA);
			}
			obsCreated.put(s, distS);
		}
		return obsCreated;
	}

	/**
	 * observation function, returns the colour of the arrival state.
	 * <p>
	 * it is possible to add noise through P_OBS_ERROR constant.
	 * 
	 * @param start  state before transition
	 * @param action performed action
	 * @param end    state after transition
	 * @return distribution over possible observations
	 */
	public Distribution<ObsMaze> computeObservation(StateMaze start, ActionMaze action, StateMaze end) {
		Distribution<ObsMaze> observs = new Distribution<ObsMaze>();

		// fetch the corresponding color from maze
		int obs = this.getMaze()[end.x][end.y];

		// add correct observation
		observs.addProbability(this.obs.get(obs), 1 - pObsError);

		// if noise possible
		if (pObsError > 0) {
			// uniform probability to have a specific color (excluding the true one)
			double proba_couleur = pObsError / (numColors - 1);
			// for each observation
			for (int nO = 0; nO < this.numColors; nO++) {
				// if color is not the right one
				if (nO != obs)
					observs.addProbability(this.obs.get(nO), proba_couleur);
			}
		}
		return observs;
	}

	@Override
	/**
	 * list of all possible states
	 */
	public List<StateMaze> allStates() {
		return states;
	}

	@Override
	/**
	 * list of all possible actions
	 */
	public List<ActionMaze> allAction() {
		return actions;
	}

	@Override
	/**
	 * list of all possible actions
	 */
	public List<ObsMaze> allObs() {
		return this.obs;
	}

	/******************* GETTER ***********************/

	public int[][] getMaze() {
		return maze;
	}

	public void setLabyrinthe(int[][] labyrinthe) {
		this.maze = labyrinthe;
	}

	@Override
	public double getGamma() {
		return 0.95;
	}

	@Override
	public Belief<StateMaze> getB0() {
		// if belief not created
		if (this.belief0 == null) {
			// uniform distribution on black cells
			belief0 = new Belief<>();
			for (StateMaze s : this.states) {
				belief0.put(s, 1);
			}
			belief0.setUniform();
		}
		// return initial belief
		return this.belief0;
	}

}
