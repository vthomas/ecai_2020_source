package problem.rhopomdp_belief_bprime.mazeLocalization;

import model.mdp.StateXY;

/**
 * maze location
 * 
 */
public class StateMaze extends StateXY {

	/**
	 * constructor
	 * 
	 * @param x position x
	 * @param y position y
	 */
	public StateMaze(int x, int y) {
		super(x, y);
	}

	/**
	 * all possible states
	 * 
	 * @param dx size x
	 * @param dy size y
	 * @return liste of states
	 */
	public static StateMaze[] allStates(int dx, int dy) {
		StateMaze[] states = new StateMaze[dx * dy];

		// building states
		int k = 0;
		for (int i = 0; i < dx; i++)
			for (int j = 0; j < dy; j++) {
				states[k] = new StateMaze(i, j);
				k++;
			}

		return states;
	}
}
