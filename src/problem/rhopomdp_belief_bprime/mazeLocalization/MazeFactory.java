package problem.rhopomdp_belief_bprime.mazeLocalization;

/**
 * generate several mazes
 * 
 * @author vthomas
 * 
 */
public class MazeFactory {

	/**
	 * generates a maze
	 * 
	 * @param name maze name
	 * @return corresponding maze
	 */
	public static int[][] maze(String name) {
		switch (name) {
		case "cross":
			return mazeCross();

		case "lines":
			return mazeLines();

		case "empty":
			return emptymaze();

		case "hole":
			return mazeHole();

		case "dots":
			return mazeDots();

		}
		System.out.println("WARNING : unknown maze");
		throw new Error("unkown maze");
	}

	/**
	 * @return description of mazedots maze
	 */
	private static int[][] mazeDots() {
		int[][] laby = new int[6][6];
		laby[1][1] = 1;
		laby[4][4] = 1;
		laby[1][4] = 1;

		return laby;
	}

	/**
	 * @return description of mazehole maze
	 */
	private static int[][] mazeHole() {
		int[][] laby = new int[8][8];
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				laby[i * 2][j * 2] = 1;
		laby[4][4] = 0;
		return laby;
	}

	/**
	 * @return description of an empty maze (with a centered grey cell)
	 */
	private static int[][] emptymaze() {
		int[][] laby = new int[7][7];
		laby[3][3] = 1;
		return laby;
	}

	/**
	 * @return description of mazeLines (2 regions one with lines, other with a
	 *         unique grey cell)
	 */
	public static int[][] mazeLines() {

		// region with lines
		int[][] laby = new int[12][6];
		for (int i = 1; i < 7; i++) {
			laby[i][1] = 1;
			laby[i][3] = 1;
		}

		for (int i = 1; i < 7; i++) {
			laby[i][5] = 1;
		}

		// region with grey cell
		laby[10][5] = 1;

		return laby;
	}

	/**
	 * @return description of Cross Maze
	 */
	private static int[][] mazeCross() {
		// maze with a cross centered in (0,0)
		int[][] laby = new int[5][5];
		for (int i = 0; i < 5; i++) {
			laby[0][i] = 1;
			laby[i][0] = 1;
		}
		return laby;
	}

}
