package problem.rhopomdp_belief_bprime.seekNseek;

import java.util.ArrayList;
import java.util.List;

import model.pomdp.ObsString;


/**
 * observation when close to prey
 *  *
 */
public class TuflucObs extends ObsString {

	public static final TuflucObs PRESENT = new TuflucObs("PRES");
	public static final TuflucObs ABSENT = new TuflucObs("ABS");
	public static final TuflucObs CLOSE = new TuflucObs("CLOS");
	public static final List<TuflucObs> ALLOBS = createObs();

	private TuflucObs(String s) {
		super(s);
	}

	/**
	 * creates list of all possible observations
	 * 
	 * @return
	 */
	private static List<TuflucObs> createObs() {
		List<TuflucObs> obs = new ArrayList<TuflucObs>();
		obs.add(PRESENT);
		obs.add(CLOSE);
		obs.add(ABSENT);

		return obs;
	}
}
