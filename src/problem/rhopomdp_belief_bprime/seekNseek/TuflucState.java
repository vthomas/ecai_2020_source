package problem.rhopomdp_belief_bprime.seekNseek;

/**
 * tufluc state is state of tufluc and state of prey
 *
 */
public class TuflucState {

	public int tufX, tufY;
	public int preyX, preyY;

	public TuflucState(int tx, int ty, int px, int py) {
		this.tufX = tx;
		this.tufY = ty;
		this.preyX = px;
		this.preyY = py;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + preyX;
		result = prime * result + preyY;
		result = prime * result + tufX;
		result = prime * result + tufY;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TuflucState other = (TuflucState) obj;
		if (preyX != other.preyX)
			return false;
		if (preyY != other.preyY)
			return false;
		if (tufX != other.tufX)
			return false;
		if (tufY != other.tufY)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TuflucState [tufX=" + tufX + ", tufY=" + tufY + ", preyX=" + preyX + ", preyY=" + preyY + "]";
	}

	/**
	 * 
	 * @return manhattan distance between tufluc and prey
	 */
	public int distanceTuflucPrey() {
		return Math.abs(this.preyX - this.tufX) + Math.abs(this.preyY - this.tufY);
	}

}
