package problem.rhopomdp_belief_bprime.seekNseek;

import java.util.ArrayList;
import java.util.List;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * rhpomdp representing simpfluc
 * <li>State = pos of robot and fxed location of prey
 * <li>Action = robot's move
 * <li>Obs = found or next or not
 * <li>Transition : just moves of robot (mur collision with walls)
 * <li>reward : entropy on belief state
 * 
 *
 */
public class RhoPOMDPSeek implements RhoPOMDP<TuflucState, TuflucAction, TuflucObs> {

	/**
	 * maze of the map
	 */
	int[][] maze;

	/**
	 * initial state robot
	 */
	int xInitRobot = 4;
	int yInitRobot = 3;

	/**
	 * list of possible states
	 */
	private List<TuflucState> states;

	///////////////////////// MAZE ////////////////////

	public RhoPOMDPSeek() {

		int[][] maze2 = {
				//
				{ 1, 1, 1, 1, 1, 1, 1, 1, 1 },
				//
				{ 1, 1, 0, 0, 0, 0, 0, 1, 1 },
				//
				{ 1, 0, 1, 1, 0, 1, 1, 0, 1 },
				//
				{ 1, 0, 1, 0, 0, 0, 1, 0, 1 },
				//
				{ 1, 0, 0, 0, 1, 0, 0, 0, 1 },
				//
				{ 1, 0, 1, 0, 0, 0, 1, 0, 1 },
				//
				{ 1, 0, 1, 1, 0, 1, 1, 0, 1 },
				//
				{ 1, 1, 0, 0, 0, 0, 0, 1, 1 },
				//
				{ 1, 1, 1, 1, 1, 1, 1, 1, 1 } };

		this.maze = maze2;

		// creates liste of all possible states
		this.states = createStates();

	}

	///////////////////////// GETTER ///////////////////

	@Override
	public List<TuflucObs> allObs() {
		return TuflucObs.ALLOBS;
	}

	@Override
	public double getGamma() {
		return 0.95;
	}

	@Override
	/**
	 * initial belief contains all possible prey location with initial robot
	 * location
	 */
	public Belief<TuflucState> getB0() {
		Belief<TuflucState> b0;
		b0 = new Belief<>();

		// for all possible prey location
		for (int i = 0; i < this.maze.length; i++)
			for (int j = 0; j < this.maze[0].length; j++) {
				// if no wall
				if (!this.wallPresent(i, j)) {
					TuflucState s = new TuflucState(this.xInitRobot, this.yInitRobot, i, j);
					b0.put(s, 1.);
				}
			}

		// uniform distribution
		b0.setUniform();
		return b0;
	}

	private boolean wallPresent(int i, int j) {
		return maze[i][j] == 1;
	}

	@Override
	/**
	 * @return list of states previously created (cf construuctor)
	 */
	public List<TuflucState> allStates() {
		return this.states;
	}

	private List<TuflucState> createStates() {
		List<TuflucState> states;
		states = new ArrayList<>();

		// tufluc position
		for (int tx = 0; tx < this.maze.length; tx++)
			for (int ty = 0; ty < this.maze[0].length; ty++) {
				// if no wall
				if (!this.wallPresent(tx, ty)) {
					// test all prey locations
					for (int i = 0; i < this.maze.length; i++)
						for (int j = 0; j < this.maze[0].length; j++) {
							TuflucState tf = new TuflucState(tx, ty, i, j);
							states.add(tf);
						}
				}
			}
		return states;
	}

	@Override
	public List<TuflucAction> allAction() {
		return TuflucAction.ALL_ACTIONS;
	}

	///////////////////////// TRANSITION ///////////////////
	@Override
	/**
	 * transition function consists only in moving tufluc but moves are prevented by
	 * wall presence
	 */
	public Distribution<TuflucState> transition(TuflucState s, TuflucAction action) {
		// ** WARNING **
		// Reward is based on deterministic transition (and known tufluc location)

		// compute location of arrival
		int dx = s.tufX;
		int dy = s.tufY;
		if (action == TuflucAction.N) {
			dy--;
		} else if (action == TuflucAction.S) {
			dy++;
		} else if (action == TuflucAction.E) {
			dx++;
		} else if (action == TuflucAction.W) {
			dx--;
		} else
			throw new Error("action " + action + "unknown");

		// if wall present, arrival is starting state
		if (this.wallPresent(dx, dy)) {
			// reinitialise dx and dy
			dx = s.tufX;
			dy = s.tufY;
		}

		// resulting distribution
		Distribution<TuflucState> result = new Distribution<>();
		TuflucState sPrime = new TuflucState(dx, dy, s.preyX, s.preyY);
		result.addProbability(sPrime, 1.);
		return result;
	}

	///////////////////////// OBSERVATION ///////////////////

	@Override
	public Distribution<TuflucObs> observation(TuflucState starting, TuflucAction action, TuflucState fin) {
		// final distribution
		Distribution<TuflucObs> res = new Distribution<>();

		// compute distance on final state
		int d = fin.distanceTuflucPrey();

		if (d == 0) {
			res.addProbability(TuflucObs.PRESENT, 1.);
		} else if (d == 1) {
			res.addProbability(TuflucObs.CLOSE, 1.);
		} else
			res.addProbability(TuflucObs.ABSENT, 1.);

		return res;
	}

	///////////////////////// REWARD ///////////////////

	@Override
	public double reward(Belief<TuflucState> dep, TuflucAction action, Belief<TuflucState> arr) {
		// WARNING WORKS SINCE MOVE IS DETERMINISTIC
		// entropy on state = entropy on prey
		return - arr.getEntropy();
	}

}
