package problem.rhopomdp_belief_bprime.tigergrid;

import model.pomdp.Belief;
import problem.rhopomdp_belief_bprime.navigation.NavMap;
import problem.rhopomdp_belief_bprime.navigation.NavState;
import problem.rhopomdp_belief_bprime.navigation.RhoPOMDPNavigation;

/**
 * tiger grid problem.
 * <p>
 * It corresponds to
 * <ul>
 * <li>1- specific wall and reward
 * <li>2- specific b0
 * </ul>
 *
 */
public class TigerGrid extends RhoPOMDPNavigation {

	/**
	 * build a tiger grid (littman 95)
	 */
	public TigerGrid() {
		super();
		// create maze (T = tiger, G=goal, S=starting)
		// XXXXXXX
		// XT G TX
		// X SXS X
		// XXXXXXX

		boolean[][] pWalls = new boolean[7][];
		pWalls[0] = new boolean[] { true, true, true, true };
		pWalls[1] = new boolean[] { true, false, false, true };
		pWalls[2] = new boolean[] { true, false, false, true };
		pWalls[3] = new boolean[] { true, false, true, true };
		pWalls[4] = new boolean[] { true, false, false, true };
		pWalls[5] = new boolean[] { true, false, false, true };
		pWalls[6] = new boolean[] { true, true, true, true };

		// create goal
		int gX = 3;
		int gY = 1;

		// create rewards
		double[][] rewards = new double[7][4];
		int tigar_rec = -10;
		rewards[1][1] = tigar_rec;
		rewards[5][1] = tigar_rec;

		// create navmap
		this.setNavmap(new NavMap(pWalls, rewards, gX, gY));
	}

	@Override
	public Belief<NavState> getB0() {
		Belief<NavState> b0 = new Belief<>();
		b0.put(NavState.getState(2, 2, NavState.NORTH), 0.5);
		b0.put(NavState.getState(4, 2, NavState.NORTH), 0.5);
		return b0;
	}

}
