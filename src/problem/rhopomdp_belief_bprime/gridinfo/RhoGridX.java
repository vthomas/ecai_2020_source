package problem.rhopomdp_belief_bprime.gridinfo;

import model.pomdp.Belief;

/**
 * rho grid where objective is to know its x location
 *
 */
public class RhoGridX extends RhoPOMDPGrid {

	@Override
	/**
	 * reward corresponds to information on x
	 */
	public double reward(Belief<StateGrid> bStart, ActionGrid action, Belief<StateGrid> bEnd) {
		// compute belief on x
		double[] pX = new double[3];

		// for each state in belief
		for (StateGrid s : bEnd.keySet()) {
			// get proba and put on right x value
			pX[s.x] += bEnd.get(s);
		}

		// compute norm 1
		double r = 0;
		for (int i = 0; i < pX.length; i++) {
			r += Math.abs(pX[i] - 1. / 3);
		}
		return r;
	}

}
