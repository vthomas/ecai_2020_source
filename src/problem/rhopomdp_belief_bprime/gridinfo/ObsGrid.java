package problem.rhopomdp_belief_bprime.gridinfo;

/**
 * Cell color observation
 * 
 */
public class ObsGrid {

	public static final ObsGrid[] OBS = { new ObsGrid(0), new ObsGrid(1) };
	
	/**
	 * observation received
	 */
	public int obs;

	/**
	 * add a new possible observation
	 * 
	 * @param i color number
	 */
	public ObsGrid(int i) {
		this.obs = i;
	}

	
	public String toString() {
		return "obs : " + this.obs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + obs;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObsGrid other = (ObsGrid) obj;
		if (obs != other.obs)
			return false;
		return true;
	}

}
