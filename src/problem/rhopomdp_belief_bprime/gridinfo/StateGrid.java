package problem.rhopomdp_belief_bprime.gridinfo;

import model.mdp.StateXY;

/**
 * location in the maze
 * 
 */
public class StateGrid extends StateXY {

	/**
	 * constructor
	 * 
	 * @param x position x
	 * @param y position y
	 */
	public StateGrid(int x, int y) {
		super(x, y);
	}

	/**
	 * all possible states
	 * 
	 * @param dx size x
	 * @param dy size y
	 * @return states list
	 */
	public static StateGrid[] allStates(int dx, int dy) {
		// possibles states
		StateGrid[] states = new StateGrid[dx * dy];

		// build states
		int k = 0;
		for (int i = 0; i < dx; i++)
			for (int j = 0; j < dy; j++) {
				states[k] = new StateGrid(i, j);
				k++;
			}

		return states;
	}
}
