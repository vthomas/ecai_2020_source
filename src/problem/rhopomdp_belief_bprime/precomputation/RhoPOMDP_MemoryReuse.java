package problem.rhopomdp_belief_bprime.precomputation;

import java.util.HashMap;
import java.util.List;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * forces to use the same states and observations
 */
public class RhoPOMDP_MemoryReuse<S, A, O> implements RhoPOMDP<S, A, O> {

	/**
	 * original rho pomdp
	 */
	RhoPOMDP<S, A, O> original;

	/**
	 * static states and observations
	 */
	HashMap<S, S> knownStates;
	HashMap<O, O> knownObs;

	/**
	 * build a rhoPOMDP with memory reuse
	 */
	public RhoPOMDP_MemoryReuse(RhoPOMDP<S, A, O> pb) {
		this.original = pb;
		this.knownObs = new HashMap<>();
		this.knownStates = new HashMap<>();
	}

	@Override
	public double reward(Belief<S> startB, A action, Belief<S> arrivB) {
		return this.original.reward(startB, action, arrivB);
	}

	@Override
	public Distribution<S> transition(S initS, A action) {
		Distribution<S> newStates = this.original.transition(initS, action);
		// distribution with known states
		Distribution<S> recast = new Distribution<>();
		// for each state
		for (S s : newStates) {
			S snew = null;
			// if state not known, add it
			if (!this.knownStates.containsKey(s)) {
				this.knownStates.put(s, s);
				snew = s;
			}
			// else get corrsponding known state
			else {
				snew = this.knownStates.get(s);
			}

			// add to recast
			recast.addProbability(snew, newStates.getProba(s));
		}
		return recast;
	}

	@Override
	public List<S> allStates() {
		return this.original.allStates();
	}

	@Override
	public List<A> allAction() {
		return this.original.allAction();
	}

	@Override
	public Distribution<O> observation(S starting, A action, S arrivS) {
		return this.original.observation(starting, action, arrivS);
	}

	@Override
	public List<O> allObs() {
		return this.original.allObs();
	}

	@Override
	public double getGamma() {
		return this.original.getGamma();
	}

	@Override
	public Belief<S> getB0() {
		return this.original.getB0();
	}

}
