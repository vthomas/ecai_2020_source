package problem.rhopomdp_belief_bprime.precomputation.test;

import model.rhopomdp_reward_bprime.RhoPOMDP;
import problem.rhopomdp_belief_bprime.RhoPOMDP_BPrime_Factory;
import problem.rhopomdp_belief_bprime.precomputation.RhoPOMDP_MemoryReuse;

public class MainTest_rocksampling88_reuse {
	
	public static void main(String[] args) {
		RhoPOMDP problem = RhoPOMDP_BPrime_Factory.getProblem("rockSampling88_bprime");
		RhoPOMDP_MemoryReuse pbReuse = new RhoPOMDP_MemoryReuse(problem);
		
	}
}
