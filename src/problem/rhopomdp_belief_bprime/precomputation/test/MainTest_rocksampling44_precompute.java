package problem.rhopomdp_belief_bprime.precomputation.test;

import model.rhopomdp_reward_bprime.RhoPOMDP;
import problem.rhopomdp_belief_bprime.RhoPOMDP_BPrime_Factory;
import problem.rhopomdp_belief_bprime.precomputation.RhoPOMDP_PreComp;

/**
 * test that rhoPOMDP precomputation works correctly
 */
public class MainTest_rocksampling44_precompute {

	public static void main(String[] args) {
		RhoPOMDP problem = RhoPOMDP_BPrime_Factory.getProblem("rockSampling44_bprime");
		
		// precompute
		RhoPOMDP_PreComp problemPre = new RhoPOMDP_PreComp<>(problem);
		
		//stop
		System.out.println("precomputed");

	}
}
