package problem.rhopomdp_belief_bprime.precomputation;

import java.util.HashMap;
import java.util.List;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * precomputes a RhoPOMDP to use the rhoPOMDP without generating new States
 */
public class RhoPOMDP_PreComp<S, A, O> implements RhoPOMDP<S, A, O> {

	RhoPOMDP<S, A, O> original;

	/**
	 * static states and observations
	 */
	HashMap<S, S> knownStates;
	HashMap<O, O> knownObs;

	/**
	 * static transition function
	 */
	HashMap<S, HashMap<A, Distribution<S>>> transition;
	HashMap<S, HashMap<A, HashMap<S, Distribution<O>>>> observations;

	/**
	 * precomputes POMDP from orginal
	 * 
	 * @param original original rhopomdp
	 */
	public RhoPOMDP_PreComp(RhoPOMDP<S, A, O> original) {
		this.original = original;

		// get all states
		List<S> allStates = original.allStates();

		// store all states to have static ref
		knownStates = new HashMap<>(allStates.size());
		for (S state : allStates)
			knownStates.put(state, state);

		// builds transition matrix
		transition = new HashMap<>();
		// for all state
		for (S s : allStates) {
			// build transition for s
			HashMap<A, Distribution<S>> transitionS = new HashMap<>();
			transition.put(s, transitionS);
			// for all actions
			for (A a : this.original.allAction()) {
				// get distribution on s'
				Distribution<S> transitions = this.original.transition(s, a);
				Distribution<S> transitionStatiq = new Distribution<S>();
				// modify s' to use the same references
				for (S sNext : transitions) {
					transitionStatiq.addProbability(this.knownStates.get(sNext), transitions.getProba(sNext));
				}
				// store in transition in transition hashmap
				transitionS.put(a, transitionStatiq);
			}
		}

		// builds static observation
		// get all observations
		List<O> listObs = this.original.allObs();
		this.knownObs = new HashMap<>(listObs.size());
		// add to known observations
		for (O obs : listObs) {
			this.knownObs.put(obs, obs);
		}

		// builds observation matrix
		this.observations = new HashMap<>();
		// for each s
		for (S s : this.original.allStates()) {
			// build s hashmap and add it to observations
			HashMap<A, HashMap<S, Distribution<O>>> observationS = new HashMap<>();
			this.observations.put(s, observationS);
			// for each a
			for (A a : this.original.allAction()) {
				// build a hashamp and add it to hashmapS
				HashMap<S, Distribution<O>> obsA = new HashMap<>();
				observationS.put(a, obsA);
				// for each sNext
				for (S sNext : this.original.allStates()) {
					// get obsDit
					Distribution<O> distObs = this.original.observation(s, a, sNext);
					Distribution<O> distObsStatic = new Distribution<O>();

					// transform for static observation
					for (O obs : distObs) {
						distObsStatic.addProbability(this.knownObs.get(obs), distObs.getProba(obs));
					}

					// add to hashmap obsA
					obsA.put(sNext, distObsStatic);
				}
			}
		}
	}

	@Override
	public Distribution<S> transition(S s, A a) {
		// use precomputed distibutions
		return this.transition.get(s).get(a);
	}

	@Override
	public Distribution<O> observation(S s, A a, S sNext) {
		// use precomputed distibutions
		return this.observations.get(s).get(a).get(sNext);
	}

	/*********************************
	 * just a reuse of original Rho-POMDP
	 ***********************************/

	@Override
	public List<S> allStates() {
		// no need to change states since they are the known states
		return original.allStates();
	}

	@Override
	public List<A> allAction() {
		// no need to change actions since they are the same
		return original.allAction();
	}

	@Override
	public List<O> allObs() {
		// no need to change observations since they are the known observations
		return original.allObs();
	}

	@Override
	public double getGamma() {
		return this.original.getGamma();
	}

	@Override
	public double reward(Belief<S> b, A action, Belief<S> arrivB) {
		// use original rhoPOMD
		return this.original.reward(b, action, arrivB);
	}

	@Override
	public Belief<S> getB0() {
		return this.original.getB0();
	}

}
