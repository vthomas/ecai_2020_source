package problem.rhopomdp_belief_bprime.rho_from_pomdp;

import java.util.List;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.pomdp.POMDP;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * builds a rhoPOMDP from a POMDP
 *
 * @param <S>
 * @param <A>
 * @param <O>
 */
public class RhoPOMDPFrom<S, A, O> implements RhoPOMDP<S, A, O> {

	/**
	 * POMDP from which everything is computed
	 */
	POMDP<S, A, O> pomdp;

	/**
	 * builds rhoPOMDP from POMDP
	 * 
	 * @param pb pomdp problem
	 */
	public RhoPOMDPFrom(POMDP<S, A, O> pb) {
		this.pomdp = pb;
	}

	@Override
	public double reward(Belief<S> startB, A action, Belief<S> arrivB) {
		double r = 0;
		// for each possible start
		for (S s : startB.keySet()) {
			// get proba
			double proba = startB.get(s);
			// get reward
			double rew = this.pomdp.r(s, action);
			r += proba * rew;
		}

		return r;
	}

	@Override
	public Distribution<S> transition(S initS, A action) {
		return pomdp.transition(initS, action);
	}

	@Override
	public List<S> allStates() {
		return pomdp.allStates();
	}

	@Override
	public List<A> allAction() {
		return pomdp.allAction();
	}

	@Override
	public Distribution<O> observation(S starting, A action, S arrivS) {
		return pomdp.observe(action, arrivS);
	}

	@Override
	public List<O> allObs() {
		return pomdp.allObs();
	}

	@Override
	public double getGamma() {
		return pomdp.getGamma();
	}

	@Override
	public Belief<S> getB0() {
		return pomdp.getB0();
	}

}
