package problem.rhopomdp_belief_bprime.rockSampling;

import java.util.Arrays;

public class StateRock {

	/**
	 * robot location
	 */
	int x, y;

	/**
	 * array of rock types
	 */
	boolean[] types;

	/**
	 * creates a copy of s
	 * 
	 * @param s
	 */
	public StateRock(StateRock s) {
		this.x = s.x;
		this.y = s.y;
		this.types = (boolean[]) (s.types.clone());
	}

	public StateRock(int x2, int y2, boolean[] typ) {
		this.x = x2;
		this.y = y2;
		this.types = typ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(types);
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StateRock other = (StateRock) obj;
		if (!Arrays.equals(types, other.types))
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	/**
	 * affichage string
	 */
	public String toString() {
		String res = "(" + x + "," + y + ")R[";
		for (boolean b : this.types)
			if (b)
				res += 1;
			else
				res += 0;
		res += "]";
		return res;
	}

}
