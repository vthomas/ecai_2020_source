package problem.rhopomdp_belief_bprime.rockSampling.print;

import problem.rhopomdp_belief_bprime.RhoPOMDP_BPrime_Factory;
import problem.rhopomdp_belief_bprime.rockSampling.RhoPOMDPRockSample;

/**
 * class to print problems
 */
public class Main_Rock_PrintProblems {

	/**
	 * print problems
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String[] problems = { "rockSampling88_bprime", "rockSampling88_bprime_v300", "rockSampling88_bprime_v400",
				"rockSampling88_bprime_v500", "rockSampling88_bprime_v600" };

		for (String probName : problems) {
			// print problem name
			System.out.println(probName);

			// get and print problem
			RhoPOMDPRockSample pb = (RhoPOMDPRockSample) RhoPOMDP_BPrime_Factory.getProblem(probName);
			System.out.println(pb.drawProblemConfig());
			System.out.println("\n");
		}

	}
}
