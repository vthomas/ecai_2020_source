package problem.rhopomdp_belief_bprime.rockSampling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Map;

/**
 * structure to access locations of rocks
 *
 */
public class RockLocations {

	/**
	 * list from rock number to location
	 */
	List<Location> rocks;

	/**
	 * map from location to Rock number
	 */
	Map<Location, Integer> locations;

	/**
	 * public locations of rocks
	 * 
	 * @param n    size of grid
	 * @param k    number of rocks
	 * @param seed random seed
	 */
	public RockLocations(int n, int k, long seed) {
		// create randomgen
		Random rgen = new Random();
		rgen.setSeed(seed);

		// build rocks
		this.rocks = new ArrayList<>();
		this.locations = new HashMap<>();

		for (int i = 0; i < k; i++) {
			// build rock number i

			// random location while location is not empty
			int x = -1;
			int y = -1;
			Location r = null;
			boolean isOccupied = true;
			while (isOccupied) {
				x = rgen.nextInt(n);
				y = rgen.nextInt(n);
				r = new Location(x, y);
				// look if cell x,y is occupied
				isOccupied = this.locations.containsKey(r);
				// if on exit
				if ((x == 0) && (y == 0))
					isOccupied = true;
			}

			// build rock
			this.rocks.add(r);
			this.locations.put(r, i);
		}
	}

	/**
	 * public locations of rocks
	 * 
	 * @param n     size of grid
	 * @param k     number of rocks
	 * @param rocks array of rock locations
	 */
	public RockLocations(int n, int[][] rocks) {
		// build rocks
		this.rocks = new ArrayList<>();
		this.locations = new HashMap<>();

		for (int i = 0; i < rocks.length; i++) {
			// build rock number i
			Location r = new Location(rocks[i][0], rocks[i][1]);

			// build rock
			this.rocks.add(r);
			this.locations.put(r, i);
		}
	}

	/**
	 * return information from location
	 * 
	 * @param x location x
	 * @param y location y
	 * @return Rock num rock from location
	 */
	int getRockFromLocation(int x, int y) {
		Location rock = new Location(x, y);
		if (this.locations.containsKey(rock)) {
			return this.locations.get(rock);
		} else
			return -1;
	}

	/**
	 * get rock location
	 * 
	 * @param numRock rock number
	 * @return array (x,y) corresponding to this rock location
	 */
	Location getLocationFromRock(int numRock) {
		return this.rocks.get(numRock);
	}

}
