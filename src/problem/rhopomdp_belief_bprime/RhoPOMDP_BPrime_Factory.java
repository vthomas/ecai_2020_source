package problem.rhopomdp_belief_bprime;

import model.pomdp.POMDP;
import model.rhopomdp_reward_bprime.RhoPOMDP;
import problem.pomdp.myparser.ParserPOMDP;
import problem.rhopomdp_belief_bprime.activeLoc.RhoPOMDPActiveLoc;
import problem.rhopomdp_belief_bprime.cameraClean.RhoPOMDPCameraDiag;
import problem.rhopomdp_belief_bprime.gridinfo.RhoGridNotX;
import problem.rhopomdp_belief_bprime.gridinfo.RhoGridX;
import problem.rhopomdp_belief_bprime.mazeLocalization.MazeFactory;
import problem.rhopomdp_belief_bprime.mazeLocalization.RhoPOMDPMaze_withoutPrecomputatioon;
import problem.rhopomdp_belief_bprime.precomputation.RhoPOMDP_MemoryReuse;
import problem.rhopomdp_belief_bprime.rho_from_pomdp.RhoPOMDPFrom;
import problem.rhopomdp_belief_bprime.rockSampling.RhoPOMDPRockSample;
import problem.rhopomdp_belief_bprime.seekNseek.RhoPOMDPSeek;
import problem.rhopomdp_belief_bprime.tiger.RhoPOMDPTiger;
import problem.rhopomdp_belief_bprime.tigergrid.TigerGrid;
import problem.rhopomdp_belief_bprime.trackingMuseum.RhoPOMDPMuseum;
import problem.rhopomdp_belief_bprime.trackingMuseum.RhoPOMDPMuseumThreshold;

public class RhoPOMDP_BPrime_Factory {

	@SuppressWarnings("rawtypes")
	public static RhoPOMDP getProblem(String problemName) {
		RhoPOMDP problem;

		switch (problemName) {

		case "activLocLines_bprime":
		case "activlocLines_bprime":
		case "activloclines_bprime":
		case "activLocines_bprime":
		case "activloc_lines_bprime":
			problem = new RhoPOMDPActiveLoc(MazeFactory.maze("lines"));
			break;

		// tiger grid from cassandra file
		case "tigerGridFile":
		case "TigerGridFile":
		case "tigergridFile":
			POMDP pomdpTiger = ParserPOMDP.readPOMDP("POMDPfile/tiger-grid.POMDP");
			problem = new RhoPOMDPFrom(pomdpTiger);
			break;

		// hallway 2 problem from cassandraFile
		case "hallway2File":
		case "Hallway2File":
		case "hallway2file":
			POMDP pomdpHallway2 = ParserPOMDP.readPOMDP("POMDPfile/hallway2.POMDP");
			problem = new RhoPOMDPFrom(pomdpHallway2);
			break;

		case "tigerGrid":
		case "tiger-grid":
		case "tigergrid":
		case "TigerGrid":
		case "gridtiger":
		case "grid-tiger":
		case "Grid-Tiger":
		case "GridTiger":
			problem = new TigerGrid();
			break;

		case "activLocCross_bprime":
		case "activlocCross_bprime":
		case "activloccross_bprime":
		case "activLoccross_bprime":
		case "activloc_cross_bprime":
			problem = new RhoPOMDPActiveLoc(MazeFactory.maze("cross"));
			break;

		case "activLocDots_bprime":
		case "activlocDots_bprime":
		case "activlocdots_bprime":
		case "activLocdots_bprime":
		case "activloc_dots_bprime":
			problem = new RhoPOMDPActiveLoc(MazeFactory.maze("dots"));
			break;

		case "activLocHole_bprime":
		case "activLochole_bprime":
		case "activlocHole_bprime":
		case "activlochole_bprime":
		case "activloc_hole_bprime":
			problem = new RhoPOMDPActiveLoc(MazeFactory.maze("hole"));
			break;

		case "seek_bprime":
			problem = new RhoPOMDPSeek();
			break;

		case "mazelines_bprime":
			problem = new RhoPOMDPMaze_withoutPrecomputatioon(MazeFactory.maze("lines"));
			break;

		case "mazecross_bprime":
			problem = new RhoPOMDPMaze_withoutPrecomputatioon(MazeFactory.maze("cross"));
			break;

		case "mazeempty_bprime":
			problem = new RhoPOMDPMaze_withoutPrecomputatioon(MazeFactory.maze("empty"));
			break;

		case "mazehole_bprime":
		case "mazeHole_bprime":
			problem = new RhoPOMDPMaze_withoutPrecomputatioon(MazeFactory.maze("hole"));
			break;

		case "mazedots_bprime":
		case "mazeDots_bprime":
			problem = new RhoPOMDPMaze_withoutPrecomputatioon(MazeFactory.maze("dots"));
			break;

		case "camera":
			problem = new RhoPOMDPCameraDiag(4);
			break;

		case "tiger_bprime":
			problem = new RhoPOMDPTiger();
			break;

		case "rockSampling44_bprime":
			problem = new RhoPOMDPRockSample(4, 4, 100);
			break;

		case "rockSampling66_bprime":
			problem = new RhoPOMDPRockSample(6, 6, 100);
			break;

		case "rockSampling88_bprime":
			problem = new RhoPOMDPRockSample(8, 8, 200);
			break;

		case "rockSampling88_reuse_bprime":
			problem = new RhoPOMDPRockSample(8, 8, 200);
			RhoPOMDP_MemoryReuse rhoPOMDP_MemoryReuse = new RhoPOMDP_MemoryReuse(problem);
			problem = rhoPOMDP_MemoryReuse;
			break;

		case "rockSampling88_bprime_v300":
			problem = new RhoPOMDPRockSample(8, 8, 300);
			break;
			
		case "rockSampling88_bprime_v300_reuse":
			problem = new RhoPOMDPRockSample(8, 8, 300);
			problem = new RhoPOMDP_MemoryReuse(problem);
			break;

		case "rockSampling88_bprime_v400":
			problem = new RhoPOMDPRockSample(8, 8, 400);
			break;
			
		case "rockSampling88_bprime_v400_reuse":
			problem = new RhoPOMDPRockSample(8, 8, 400);
			problem = new RhoPOMDP_MemoryReuse(problem);
			break;

		case "rockSampling88_bprime_v500":
			problem = new RhoPOMDPRockSample(8, 8, 500);
			break;

		case "rockSampling88_bprime_v600":
			problem = new RhoPOMDPRockSample(8, 8, 600);
			break;

		case "museum_bprime":
			problem = new RhoPOMDPMuseum(4, 4);
			break;

		case "museum_threshold_08":
		case "museumThreshold_08":
		case "MuseumThreshold_08":
			problem = new RhoPOMDPMuseumThreshold(4, 4, 0.8);
			break;

		case "rockSampling66":
			problem = new RhoPOMDPRockSample(6, 6, 100);
			break;

		case "gridnotx_bprime":
		case "gridnotX_bprime":
		case "gridNotX_bprime":
			problem = new RhoGridNotX();
			break;

		case "gridx_bprime":
			problem = new RhoGridX();
			break;

		default:
			throw new Error("pb does not exist");
			// problem = null;
			// break;
		}
		return problem;
	}

}
