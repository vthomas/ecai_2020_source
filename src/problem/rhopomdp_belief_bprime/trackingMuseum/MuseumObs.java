package problem.rhopomdp_belief_bprime.trackingMuseum;

import java.util.ArrayList;
import java.util.List;

import model.pomdp.ObsString;

public class MuseumObs extends ObsString {

	public static final MuseumObs PRESENT = new MuseumObs("PRES");
	public static final MuseumObs ABSENT = new MuseumObs("ABS");
	public static final MuseumObs CLOSE = new MuseumObs("CLOS");
	public static final List<MuseumObs> ALLOBS = createObs();

	private MuseumObs(String s) {
		super(s);
	}

	/**
	 * creates list of all possible observations
	 * 
	 * @return
	 */
	private static List<MuseumObs> createObs() {
		List<MuseumObs> obs = new ArrayList<MuseumObs>();
		obs.add(PRESENT);
		obs.add(CLOSE);
		obs.add(ABSENT);

		return obs;
	}

}
