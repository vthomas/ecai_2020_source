package problem.rhopomdp_belief_bprime.trackingMuseum;

/**
 * create action
 *
 */
public class MuseumAction {

	/**
	 * attributes of action (activated camera location)
	 */
	int x, y;

	MuseumAction(int px, int py) {
		this.x = px;
		this.y = py;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MuseumAction other = (MuseumAction) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	public String toString() {
		return "(" + this.x + "," + this.y + ")";
	}

}
