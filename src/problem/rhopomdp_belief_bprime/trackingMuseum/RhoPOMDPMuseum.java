package problem.rhopomdp_belief_bprime.trackingMuseum;

import java.util.ArrayList;
import java.util.List;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * in museum problem,
 * <ul>
 * <li>one child is roaming in a museum according to a stochastic strategy.
 * <li>the agent can activate a camera on each cell which gives information
 * about the location of the child
 * <li>the reward is the difference of entropy of the probability over the
 * child's location
 * </ul>
 * 
 * in other words,
 * <ul>
 * <li>world = grid n x n
 * <li>state = location of child
 * <li>action = location of activated camera
 * <li>observation = Present , Absent, Close (next to the activated camera)
 * <li>state transition = child movement (0.6 staying, 0.1 on each direction)
 * <li>observation process = deterministic
 * </ul>
 *
 */
public class RhoPOMDPMuseum implements RhoPOMDP<MuseumState, MuseumAction, MuseumObs> {

	/**
	 * the size of the world
	 */
	private int sizeGridX, sizeGridY;

	/**
	 * the states
	 */
	private List<MuseumState> allStates = null;

	/**
	 * the actions
	 */
	private List<MuseumAction> actions = null;

	/////////////////// BUILDER RhoPOMDP Museum ///////////////////////

	/**
	 * creates a museum
	 * 
	 * @param n size of museum
	 */
	public RhoPOMDPMuseum(int tx, int ty) {
		this.sizeGridX = tx;
		this.sizeGridY = ty;

		this.allStates = this.createStates();
		this.actions = this.createActions();
	}

	/**
	 * creates the state list
	 * 
	 * @return list of possible states
	 */
	private List<MuseumState> createStates() {
		List<MuseumState> result = new ArrayList<MuseumState>();
		for (int i = 0; i < this.sizeGridX; i++)
			for (int j = 0; j < this.sizeGridY; j++)
				result.add(new MuseumState(i, j));
		return result;
	}

	/**
	 * creates list of possible actions
	 * 
	 * @return list of possible actions
	 */
	private List<MuseumAction> createActions() {
		List<MuseumAction> result = new ArrayList<MuseumAction>();
		for (int i = 0; i < this.sizeGridX; i++)
			for (int j = 0; j < this.sizeGridY; j++)
				result.add(new MuseumAction(i, j));
		return result;
	}

	/////////////// GETTER /////////////////////////////////

	/**
	 * return list of states
	 */
	@Override
	public List<MuseumState> allStates() {
		return this.allStates;
	}

	/**
	 * return list of actions
	 */
	@Override
	public List<MuseumAction> allAction() {
		return this.actions;
	}

	@Override
	public List<MuseumObs> allObs() {
		return MuseumObs.ALLOBS;
	}

	@Override
	public double getGamma() {
		// TODO Auto-generated method stub
		return 0.95;
	}

	@Override
	public Belief<MuseumState> getB0() {
		// initial uniform belief
		Belief<MuseumState> b0 = new Belief<>(this.allStates);
		b0.setUniform();
		return b0;
	}

	///////////// rho POMDP transition ///////////////////////////////////////

	@Override
	public Distribution<MuseumState> transition(MuseumState s, MuseumAction action) {
		// resulting distribution
		Distribution<MuseumState> transition = new Distribution<>();

		// distribution depends on the child moves
		double probaStay = 0.6;
		double probaMove = (1 - probaStay) / 4;

		// no move
		transition.addProbability(s, probaStay);

		// for each move
		char[] moves = { 'N', 'S', 'E', 'W' };
		for (char move : moves) {
			MuseumState sArrival = this.move(move, s);
			transition.addProbability(sArrival, probaMove);
		}

		return transition;
	}

	/**
	 * method for finding next state
	 * 
	 * @param move orientation of move
	 * @return new museum state
	 */
	private MuseumState move(char move, MuseumState start) {
		int dx = 0, dy = 0;
		switch (move) {
		case 'N':
			dy = -1;
			break;
		case 'S':
			dy = 1;
			break;
		case 'W':
			dx = 1;
			break;
		case 'E':
			dx = -1;
			break;
		default:
			throw new Error("inexisting move " + move);
		}

		// new state (+toric world)
		int x = (start.x + dx + this.sizeGridX) % this.sizeGridX;
		int y = (start.y + dy + this.sizeGridY) % this.sizeGridY;
		MuseumState newS = new MuseumState(x, y);
		return newS;
	}

	///////////// rho POMDP observation ///////////////////////////////////////

	@Override
	public Distribution<MuseumObs> observation(MuseumState starting, MuseumAction action, MuseumState fin) {
		// compute distance between starting and action (with toric world)
		int d = 0;
		{
			int dx = (action.x + 2 * this.sizeGridX - fin.x) % this.sizeGridX;
			if (dx > this.sizeGridX / 2)
				dx = dx - this.sizeGridX;
			dx = Math.abs(dx);
			d = dx;

		}
		{
			int dy = (action.y + 2 * this.sizeGridY - fin.y) % this.sizeGridY;
			if (dy > this.sizeGridY / 2)
				dy = dy - this.sizeGridY;
			dy = Math.abs(dy);
			d = d + dy;
		}

		MuseumObs obs = null;
		// if distance == 0 ==> PRESENT
		if (d == 0) {
			obs = MuseumObs.PRESENT;
		}
		// if distance == 1 ==> CLOSE
		else if (d == 1) {
			obs = MuseumObs.CLOSE;
		}
		// else ==> ABSENT
		else {
			obs = MuseumObs.ABSENT;
		}
		Distribution<MuseumObs> res = new Distribution<>();
		res.addProbability(obs, 1.0);

		return res;
	}

	////////////// rho POMDP reward ///////////////////////////////////////////

	@Override
	public double reward(Belief<MuseumState> bStart, MuseumAction action, Belief<MuseumState> bEnd) {
		// difference of negentropy
		// double startNegentropy = -bStart.getEntropy();
		double endNegentropy = -bEnd.getEntropy();
		return endNegentropy;
	}

}
