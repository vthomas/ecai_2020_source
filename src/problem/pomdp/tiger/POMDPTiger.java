package problem.pomdp.tiger;

import java.util.Arrays;
import java.util.List;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.pomdp.POMDP;

/**
 * Tiger POMDP problem
 * 
 * @author vthomas
 * 
 */
public class POMDPTiger implements POMDP<StateTigre, ActionTiger, ObservationTiger> {

	/**************** PARAMETERS OF TIGER PROBLEM ********************/

	/**
	 * gamma factor
	 */
	private static final double GAMMA = 0.75;

	/**
	 * probabilities constant
	 */
	private static final double PROBA_CORRECT_LISTEN = 0.85;

	/**
	 * constant rewards used
	 */
	private static final int REWARD_TIGER = -100;
	private static final int REWARD_TREASURE = +10;
	private static final int REWARD_LISTEN = -1;

	/********************** TIGER PROBLEM **************************/

	/**
	 * set of possible actions
	 */
	List<ActionTiger> actions;

	/**
	 * state space
	 */
	List<StateTigre> states;

	/**
	 * observation space
	 */
	List<ObservationTiger> obs;

	/**
	 * builds a pomdp Tiger problem, initialize sets
	 */
	public POMDPTiger() {
		actions = Arrays.asList(ActionTiger.ACTIONS);
		states = Arrays.asList(StateTigre.states);
		obs = Arrays.asList(ObservationTiger.OBS);
	}

	@Override
	/**
	 * transition function
	 */
	public Distribution<StateTigre> transition(StateTigre s, ActionTiger action) {
		// if open door, new state with probability 0.5
		if ((action == ActionTiger.OPEN_LEFT) || (action == ActionTiger.OPEN_RIGHT)) {
			Distribution<StateTigre> dist = new Distribution<>();
			dist.addProbability(StateTigre.LEFT, 0.5);
			dist.addProbability(StateTigre.RIGHT, 0.5);
			return (dist);
		}

		// if listen, same state
		Distribution<StateTigre> dist = new Distribution<StateTigre>();
		dist.addProbability(s, 1);
		return dist;
	}

	/**
	 * observation function
	 */
	@Override
	public Distribution<ObservationTiger> observe(ActionTiger action, StateTigre endState) {
		// if open, any observation
		if ((action == ActionTiger.OPEN_LEFT) || (action == ActionTiger.OPEN_RIGHT)) {
			Distribution<ObservationTiger> dist = new Distribution<>();
			dist.addProbability(ObservationTiger.NOISE_RIGHT, 0.5);
			dist.addProbability(ObservationTiger.NOISE_LEFT, 0.5);
			return (dist);
		}

		// if listening
		if (action == ActionTiger.LISTEN) {
			double correctProbability = PROBA_CORRECT_LISTEN;
			if (endState.equals(StateTigre.LEFT)) {
				// tiger left
				Distribution<ObservationTiger> dist = new Distribution<ObservationTiger>();
				dist.addProbability(ObservationTiger.NOISE_LEFT, correctProbability);
				dist.addProbability(ObservationTiger.NOISE_RIGHT, 1 - correctProbability);
				return dist;
			} else {
				// tiger right
				Distribution<ObservationTiger> dist = new Distribution<ObservationTiger>();
				dist.addProbability(ObservationTiger.NOISE_LEFT, 1 - correctProbability);
				dist.addProbability(ObservationTiger.NOISE_RIGHT, correctProbability);
				return dist;
			}
		}

		// unknown action
		throw new Error("Action unknown " + action);
	}

	@Override
	public double r(StateTigre s, ActionTiger a) {
		// if listening
		if (a == ActionTiger.LISTEN) {
			// cost -1
			return REWARD_LISTEN;
		}

		// if open left
		if (a == ActionTiger.OPEN_LEFT) {
			// if tiger left
			if (s.equals(StateTigre.LEFT))
				return REWARD_TIGER;
			else {
				// treasure
				return REWARD_TREASURE;
			}

		}

		// if open right
		if (a == ActionTiger.OPEN_RIGHT) {
			// if tiger right
			if (s.equals(StateTigre.RIGHT))
				return REWARD_TIGER;
			else {
				// treasure
				return REWARD_TREASURE;
			}
		}

		return 0;
	}

	@Override
	public List<StateTigre> allStates() {
		return this.states;
	}

	@Override
	public List<ActionTiger> allAction() {
		return this.actions;
	}

	@Override
	public List<ObservationTiger> allObs() {
		return this.obs;
	}

	@Override
	public double getGamma() {
		return GAMMA;
	}

	@Override
	public Belief<StateTigre> getB0() {
		// initial belief
		Belief<StateTigre> b0 = new Belief<>(this.allStates());
		b0.setUniform();
		return b0;
	}

}
