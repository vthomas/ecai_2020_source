package problem.pomdp.tiger;

import model.mdp.StateAbstract;

/**
 * tiger state (location of tigre) 
 */
public class StateTigre extends StateAbstract {

	/**
	 * possible states 
	 */
	public static StateTigre LEFT = new StateTigre("TLeft");
	public static StateTigre RIGHT = new StateTigre("TRight");
	public static StateTigre[] states = { LEFT, RIGHT };

	/**
	 * state name 
	 */
	String nameState;

	/**
	 * state creation
	 */
	public StateTigre(String s) {
		this.nameState = s;
	}

	/**
	 * @return possible state list
	 */
	public StateTigre[] etats() {
		return states;
	}

	@Override
	public int hashCode() {
		return this.nameState.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		StateTigre etat = (StateTigre) obj;
		return etat.nameState.equals(this.nameState);
	}

	public String toString() {
		return this.nameState;
	}

}