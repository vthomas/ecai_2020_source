package problem.pomdp.myparser;

import java.io.OutputStream;
import java.io.PrintStream;

/**********************************************
 * Parser inspired by SolvePOMDP from Erwin Walraven
 * Uses libpomdp
 * 
 *******************************************/

import java.util.HashMap;

import libpomdp.common.Pomdp;
import libpomdp.parser.FileParser;
import model.mdp.Distribution;

public class ParserPOMDP {

	public static final boolean DEBUG = false;
	public static final boolean NO_INFO = true;

	/**
	 * Parse a .POMDP file and create POMDP object
	 * 
	 * @param filename full path to the .POMDP file
	 * @return a POMDP object
	 */
	public static POMDPFile readPOMDP(String filename) {
		if (DEBUG) {
			System.out.println();
			System.out.println("=== READ POMDP FILE ===");
			System.out.println("File: " + filename);
		}

		// to prevent standard output due to libpomdp lib
		PrintStream save = System.out;
		if (NO_INFO) {
			System.setOut(new PrintStream(new OutputStream() {
				public void write(int b) {
					// DO NOTHING
				}
			}));
		}
		Pomdp pomdp = FileParser.loadPomdp(filename, 0);
		if (NO_INFO) {
			System.setOut(save);
		}

		int nStates = pomdp.nrStates();
		int nActions = pomdp.nrActions();
		int nObservations = pomdp.nrObservations();
		double discountFactor = pomdp.getGamma();

		double[][] rewardFunction = new double[nStates][nActions];
		HashMap<Integer, String> actionLabels = new HashMap<Integer, String>();

		// transition matrix
		Distribution<Integer>[][] transition = new Distribution[nStates][nActions];
		// for each start state, action
		for (int s = 0; s < nStates; s++)
			for (int action = 0; action < nActions; action++) {
				// build corresponding distribution
				Distribution<Integer> d = new Distribution<>();
				for (int sNext = 0; sNext < nStates; sNext++) {
					double proba = pomdp.getTransitionTable(action).get(s, sNext);
					if (proba > 0)
						d.addProbability(sNext, proba);
				}
				// add distribution
				transition[s][action] = d;
			}

		// observation matrix
		Distribution<Integer>[][] observation = new Distribution[nActions][nStates];
		for (int a = 0; a < nActions; a++) {
			for (int sNext = 0; sNext < nStates; sNext++) {
				// build corresponding distribution
				Distribution<Integer> dObs = new Distribution<>();
				for (int o = 0; o < nObservations; o++) {
					double pObs = pomdp.getObservationTable(a).get(sNext, o);
					if (pObs > 0) {
						dObs.addProbability(o, pObs);
					}

				}
				// store distribution
				observation[a][sNext] = dObs;
			}
		}

		// reward
		for (int s = 0; s < nStates; s++) {
			for (int a = 0; a < nActions; a++) {
				rewardFunction[s][a] = pomdp.getRewardTable(a).get(s);
			}
		}

		// actions label
		for (int a = 0; a < nActions; a++) {
			try {
				actionLabels.put(a, pomdp.getActionString(a));
			} catch (NullPointerException e) {
				actionLabels.put(a, a + "");
			}
		}

		// b0
		double[] beliefEntries = pomdp.getInitialBeliefState().getPoint().getArray();

		// build POMDPFile
		return new POMDPFile(nStates, nActions, nObservations, discountFactor, rewardFunction, transition, observation,
				actionLabels, beliefEntries);
	}
}