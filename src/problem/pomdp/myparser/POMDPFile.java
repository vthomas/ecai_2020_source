package problem.pomdp.myparser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import model.mdp.Distribution;
import model.pomdp.*;

/**
 * A POMDP built from a Tony Cassandra File
 */

public class POMDPFile implements POMDP<Integer, Integer, Integer> {

	/**
	 * List of entities : POMDP description
	 */
	List<Integer> states;
	List<Integer> actions;
	List<Integer> observations;

	/**
	 * POMDP functions
	 */
	double[][] rewardFunction;
	Distribution<Integer>[][] transition;
	Distribution<Integer>[][] observation;
	Belief<Integer> initialBelief;

	/**
	 * discount factor
	 */
	double gamma;

	/**
	 * number of states, actions and observations
	 */
	int nbStates, nbActions, nbObs;

	/**
	 * builds a POMDP
	 * 
	 * @param nS              number of states
	 * @param nA             number of actions
	 * @param nO                number of observations
	 * @param discount            discount factor
	 * @param transitionFunction  transition matrix
	 * @param observationFunction observation matrix
	 * @param actionLabels        names for actions
	 * @param b0                  initial belief
	 */
	public POMDPFile(int nS, int nA, int nO, double discount, double[][] reward,
			Distribution<Integer>[][] transitionFunction, Distribution<Integer>[][] observationFunction,
			HashMap<Integer, String> actionLabels, double[] b0) {
		// set sizes
		this.nbStates = nS;
		this.nbActions = nA;
		this.nbObs = nO;

		// discount
		this.gamma = discount;

		// creates list of entities
		this.states = new ArrayList<Integer>();
		for (int i = 0; i < nS; i++)
			this.states.add(i);
		this.observations = new ArrayList<Integer>();
		for (int i = 0; i < nO; i++)
			this.observations.add(i);
		this.actions = new ArrayList<Integer>();
		for (int i = 0; i < nA; i++)
			this.actions.add(i);

		// reward function
		this.rewardFunction = reward;

		// transition matrix
		this.transition = transitionFunction;

		// observation
		this.observation = observationFunction;

		// initial belief
		this.initialBelief = new Belief<>();
		for (int s = 0; s < this.nbStates; s++) {
			if (b0[s] > 0) {
				this.initialBelief.put(s, b0[s]);
			}
		}
	}

	/*******************************
	 * getter
	 ********************************/

	@Override
	public double r(Integer initS, Integer action) {
		return rewardFunction[initS][action];
	}

	@Override
	public Distribution<Integer> transition(Integer s, Integer action) {
		return (transition[s][action]);
	}

	@Override
	public Distribution<Integer> observe(Integer action, Integer arrivS) {
		return observation[action][arrivS];
	}

	@Override
	public double getGamma() {
		return gamma;
	}

	@Override
	public List<Integer> allStates() {
		return this.states;
	}

	@Override
	public List<Integer> allAction() {
		return this.actions;
	}

	@Override
	public List<Integer> allObs() {
		return this.observations;
	}

	public Belief<Integer> getB0() {
		return this.initialBelief;
	}

}
