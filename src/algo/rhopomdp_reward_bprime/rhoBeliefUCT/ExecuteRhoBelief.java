package algo.rhopomdp_reward_bprime.rhoBeliefUCT;

import algo.pomdp.pomcp.NodeActionPOMCP;
import algo.pomdp.pomcp.NodeBeliefPOMCP;
import algo.rhopomdp_reward_bprime.ExecuteRhoPOMDP;
import algo.rhopomdp_reward_bprime.rhoBeliefUCT.rollout.RolloutBeliefUCT;
import algo.rhopomdp_reward_bprime.rhopomcp.rollout.Rollout;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * launch a rhoBeliefUCT algorithm.
 * <p>
 * each object contains
 * <li>an algorithm rhobelief uct;
 * <li>the way to execute this algorithm.
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public class ExecuteRhoBelief<S, A, O> implements ExecuteRhoPOMDP<S, A, O> {

	/****************************
	 * ALGO PARAMETERS
	 *****************************/

	/**
	 * constant ucb
	 */
	double ucbCst;

	/**
	 * nb of descents before performing an action
	 */
	int descents;

	/**
	 * rollout policy
	 */
	RolloutBeliefUCT<S, A, O> rollout;

	/**
	 * boolean if subtree is kept after performing an action
	 */
	private boolean subtreeKept = true;

	/**
	 * boolean if select randomly action among best (to prevent bias)
	 */
	private boolean randomSelect = false;

	/****************************
	 * END ALGO PARAMETERS
	 *****************************/

	/**
	 * rhobelief-UCT algorithm used
	 */
	private RhoBeliefUCT<S, A, O> rhobelief;

	/**
	 * problem to solve
	 */
	private RhoPOMDP<S, A, O> problem;

	/**
	 * create an object to execution rho-beliefUCT
	 * 
	 * @param pb     problem to solve
	 * @param roll   rollout policy to use
	 * @param ucb    UCB exploration constant
	 * @param nbDesc number of descents before performing an action
	 */
	public ExecuteRhoBelief(RhoPOMDP<S, A, O> pb, RolloutBeliefUCT<S, A, O> roll, double ucb, int nbDesc) {
		this.problem = pb;
		this.rollout = roll;
		this.ucbCst = ucb;
		this.descents = nbDesc;
	}

	/**
	 * 
	 * @param pb           problem to solve
	 * @param roll         rollout policy
	 * @param ucb          UCB exploration
	 * @param nbDesc       number of descents
	 * @param randomSelect random best action (to reduce bias in action selection)
	 */
	public ExecuteRhoBelief(RhoPOMDP pb, RolloutBeliefUCT roll, double ucb, int nbDesc, boolean randomSelect) {
		this.problem = pb;
		this.rollout = roll;
		this.ucbCst = ucb;
		this.descents = nbDesc;
		this.randomSelect = randomSelect;
	}

	@Override
	public void initializeBelief(Belief<S> b) {
		// create a new algorithm (depends on randomSelect)
		if (!randomSelect) {
			this.rhobelief = new RhoBeliefUCT<>(b, this.ucbCst, this.descents, this.problem);
		} else {
			this.rhobelief = new RhoBeliefUCT_RandomEq<>(b, this.ucbCst, this.descents, this.problem);
		}
		
		// change rollout
		this.rhobelief.setRollout(this.rollout);
	}

	@Override
	public A getAction() {
		// return selected action
		return this.rhobelief.search();
	}

	@Override
	public void executeAction(A action, O observation) {
		// executing an action and receiving an observation
		// leads to change of the tree root
		this.rhobelief.modifyBeliefExecution(action, observation, this.subtreeKept);
	}

	@Override
	public String getDescription() {
		String desc = "nbDescents;";
		for (A a : this.problem.allAction())
			desc += a + ";";
		return desc;
	}

	@Override
	public String getInformation() {
		NodeBeliefPOMCP<S, A, O> root = this.rhobelief.root;
		String info = root.getVisit() + ";";
		for (A a : this.problem.allAction()) {
			NodeActionPOMCP<S, A, O> nodeActionPOMCP = root.actionChildren.get(a);
			info += nodeActionPOMCP.getNbVisits() + ";";
		}
		return info;
	}

}
