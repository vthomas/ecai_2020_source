package algo.rhopomdp_reward_bprime.rhoBeliefUCT;

import algo.pomdp.pomcp.Bag;
import algo.pomdp.pomcp.NodeActionPOMCP;
import algo.pomdp.pomcp.NodeBeliefPOMCP;
import algo.rhopomdp_reward_bprime.rhoBeliefUCT.rollout.RolloutBeliefUCT;
import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * RhoBelief-UCT algorithm (based on UCT applied to beliefMDP)
 * 
 * <p>
 * uses POMCP node classes
 */

public class RhoBeliefUCT<S, A, O> {

	/**
	 * EPSILON for cutting tree descents (when $\gamma^{depth}<\epsilon$)
	 */
	public static final double EPSILON = 0.01;

	/**
	 * show debug or not
	 */
	public static boolean PRINT_DEBUG = false;

	/**
	 * rhopomdp problem to solve
	 */
	RhoPOMDP<S, A, O> problem;

	/**
	 * belief tree (root contains children)
	 */
	NodeBeliefPOMCP<S, A, O> root;

	/**
	 * initial belief
	 */
	private Belief<S> initialB;

	/**
	 * number of descents
	 */
	private int nbDescents;

	/**
	 * rollout policy
	 */
	private RolloutBeliefUCT<S, A, O> rollout;

	/**
	 * constant UCB
	 */
	private double ucbCst;

	/**
	 * construct a RhoBeliefUCT
	 * 
	 * @param beliefInitial initial belief
	 * @param UCB           UCB constant
	 * @param descents      nb descents
	 * @param pb            problem to solve
	 */
	public RhoBeliefUCT(Belief<S> beliefInitial, double UCB, int descents, RhoPOMDP<S, A, O> pb) {
		this.ucbCst = UCB;
		this.nbDescents = descents;
		this.problem = pb;
		this.initialB = beliefInitial;

		// create root node
		root = new NodeBeliefPOMCP<S, A, O>();
		this.root.initialize(problem.allAction());

		// initialize belief root with initial belief
		Bag<S> b0 = this.root.getBag();
		for (S s : this.initialB.keySet())
			b0.addToBag(s, this.initialB.get(s));
	}

	/**
	 * @param roll rollout used for rhobelief-UCT
	 */
	public void setRollout(RolloutBeliefUCT<S, A, O> roll) {
		this.rollout = roll;
	}

	/**
	 * perform a UCT search
	 * 
	 * @param initial belief
	 * @return selected action to perform
	 */
	public A search() {
		// TASK (3) - Write Rho-Belief-UCT

		// perform descents
		for (int i = 0; i < this.nbDescents; i++) {

			// sampling state from belief
			S initialState = this.initialB.sample();
			// simulate from initial state
			this.simulate(initialState, this.root, 0);
		}
		// return best action
		A bestAction = this.getBestAction();
		return bestAction;
	}

	/**
	 * get best action once all descents have been performed
	 * 
	 * @return best action from root
	 */
	protected A getBestAction() {
		if (PRINT_DEBUG)
			System.out.println("----old method");
		
		// initialize max
		A best = null;
		double max = -Double.MAX_VALUE;

		// get best Action from root
		for (A action : this.root.actionChildren.keySet()) {

			double Vha = this.root.actionChildren.get(action).getValue();
			if (PRINT_DEBUG) {
				System.out.println(action + "->" + Vha);
			}

			if (Vha > max) {
				max = Vha;
				best = action;
			}
		}

		// return best action
		return best;
	}

	/**
	 * execute a descent from root node.
	 * 
	 * @param trajState state during trajectory
	 * @param node      current beliefnode
	 * @param depth     depth of the descent
	 * @return cumulated reward from descent
	 */
	public double simulate(S trajState, NodeBeliefPOMCP<S, A, O> node, int depth) {
		// if too much depth
		if (Math.pow(this.problem.getGamma(), depth) < EPSILON) {
			// cut the descent
			return 0;
		}

		// if node does not exist
		if (node.getVisit() == -1) {
			// initialize visit
			node.setVisit(0);

			// build children action nodes
			for (A action : this.problem.allAction()) {
				node.actionChildren.put(action, new NodeActionPOMCP<>(action));
			}

			// return rollout
			return rollout(node, depth);
		}

		// select action
		A selectedAction = this.getUCBAction(node);

		// get observation z by simulation
		Distribution<S> distState = this.problem.transition(trajState, selectedAction);
		S SPrime = distState.sample();
		Distribution<O> distObs = this.problem.observation(trajState, selectedAction, SPrime);
		O z = distObs.sample();

		// if corresponding child does not exist
		if (!node.exists(selectedAction, z)) {
			// compute corresponding arrival belief
			Bag<S> BPrime = computeBelief(node.getBag(), selectedAction, z, this.problem);

			// add child
			NodeBeliefPOMCP<S, A, O> newNode = new NodeBeliefPOMCP<>();
			newNode.setBelief(BPrime);
			node.addNode(selectedAction, z, newNode);

			// nbvisit=-1
			newNode.setVisit(-1);
		}

		// compute immediate rho
		Belief<S> dep = new Belief<>(node.getBag());
		NodeBeliefPOMCP<S, A, O> nodeArriv = node.get(selectedAction, z);
		Belief<S> arr = new Belief<>(nodeArriv.getBag());
		double rho = this.problem.reward(dep, selectedAction, arr);

		// R <- call simulate (haz)
		double simulate = this.simulate(SPrime, nodeArriv, depth + 1);
		double R = rho + this.problem.getGamma() * (simulate);

		// update Visit
		node.addVisit();
		node.addVisit(selectedAction);

		// update V(ha)
		NodeActionPOMCP<S, A, O> nAction = node.actionChildren.get(selectedAction);
		double Vha = nAction.getValue();
		double newVha = Vha + (R - Vha) / nAction.getNbVisits();
		nAction.setValue(newVha);

		return R;
	}

	/**
	 * rollout step
	 * 
	 * @param node  belief node from which performing rollout
	 * @param depth current depth
	 * @return estimated value coming from rollout policy
	 */
	private double rollout(NodeBeliefPOMCP<S, A, O> node, int depth) {
		// different from POMCP since rollout depends on estimated belief
		return this.rollout.rollout(node.getBag(), depth, this.problem);
	}

	/**
	 * compute new bag corresponding to the update of belief state
	 * <p>
	 * note that in this case a bag corresponds to a belief (sum = 1.)
	 * 
	 * @param prior          previous bag
	 * @param selectedAction performed action
	 * @param z              received observation
	 * @return belief update from bag with selected action after receiving
	 *         observation z
	 */
	public static <S, A, O> Bag<S> computeBelief(Bag<S> prior, A selectedAction, O o, RhoPOMDP<S, A, O> pb) {
		// compute P(s'|O)

		// due to obs ~ P(o|s,a,s')
		// P(s'|o) = Sum_{s,s'} (P(o|s,a,s') . P(s'|s,a) . P(s))
		Bag<S> result = new Bag<S>();

		// for each s
		for (S s : prior.keySet()) {
			double p_s = prior.get(s);

			// for each arrival state
			Distribution<S> arr = pb.transition(s, selectedAction);
			for (S s_prime : arr) {
				// get t(s',a,s)
				double t_s_a_sP = arr.getProba(s_prime);

				// compute p(o|s')
				double p_o = pb.observation(s, selectedAction, s_prime).getProba(o);

				// compute contribution to P(s'|o) ~ P(o|s,a,s') . P(s'|s,a) . P(s)
				double contrib = p_s * t_s_a_sP * p_o;
				result.addToBag(s_prime, contrib);
			}
		}

		// normalize bag (by P(O))
		result.normalize();
		return result;
	}

	/**
	 * UCB selection method
	 * 
	 * @param node from which to select
	 * @return selected action
	 */
	private A getUCBAction(NodeBeliefPOMCP<S, A, O> node) {
		A actionMax = null;
		double valueMAx = -Double.MAX_VALUE;

		// get action that maximizes ucb
		for (A action : this.problem.allAction()) {
			// get value and visit
			double actionValue = node.actionChildren.get(action).getValue();
			double visits = node.actionChildren.get(action).getNbVisits();

			// if not visited, return action
			if (visits == 0)
				return action;

			// ucb
			double ucb = actionValue + this.ucbCst * Math.sqrt(Math.log(node.getVisit()) / visits);
			if (ucb > valueMAx) {
				valueMAx = ucb;
				actionMax = action;
			}
		}
		return actionMax;

	}

	/**
	 * modify current state when performing an action
	 * 
	 * @param action      action performed
	 * @param observation observation received
	 * @param subtreeKept keep subtree
	 */
	public void modifyBeliefExecution(A action, O obs, boolean subtreeKept) {
		// get nodechild hao
		NodeBeliefPOMCP<S, A, O> selectedNode = this.root.get(action, obs);

		// modify initial belief
		this.initialB = new Belief<>(selectedNode.getBag());

		// new root reinitialize root
		if (subtreeKept) {
			this.root = selectedNode;
		} else {
			this.root = new NodeBeliefPOMCP<>();
			this.root.initialize(problem.allAction());

			// initialise belief root
			Bag<S> b0 = this.root.getBag();
			for (S s : this.initialB.keySet())
				b0.addToBag(s, this.initialB.get(s));
		}
	}

}
