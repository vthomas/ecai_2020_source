package algo.rhopomdp_reward_bprime.rhoBeliefUCT.rollout;

import algo.pomdp.pomcp.Bag;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * interface for rollouts for rho-beliefUCT.
 * 
 * @author vthomas
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public interface RolloutBeliefUCT<S, A, O> {

	/**
	 * execute rollout policy and returns value estimate
	 * 
	 * @param bag     initial belief from which excuting rollout policy
	 * @param depth   current depth
	 * @param problem rhopomdp to solve
	 * @return value estimate from initial belief
	 */
	double rollout(Bag<S> bag, int depth, RhoPOMDP<S, A, O> problem);

}
