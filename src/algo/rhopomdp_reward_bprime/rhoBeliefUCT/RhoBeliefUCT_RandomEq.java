package algo.rhopomdp_reward_bprime.rhoBeliefUCT;

import java.util.ArrayList;
import java.util.List;

import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;
import tools.RandomGen;

/**
 * RhoBelief-UCT algorithm (based on UCT applied to beliefMDP)
 * 
 * <p>
 * uses POMCP node classes
 */

public class RhoBeliefUCT_RandomEq<S, A, O> extends RhoBeliefUCT<S, A, O> {

	/**
	 * construct a RhoBeliefUCT
	 * 
	 * @param beliefInitial initial belief
	 * @param UCB           UCB constant
	 * @param descents      nb descents
	 * @param pb            problem to solve
	 */
	public RhoBeliefUCT_RandomEq(Belief<S> beliefInitial, double UCB, int descents, RhoPOMDP<S, A, O> pb) {
		super(beliefInitial, UCB, descents, pb);
		if (PRINT_DEBUG)
			System.out.println("random action selection");
	}

	/**
	 * get best action among several identical actions
	 * 
	 * @return best action from root
	 */
	protected A getBestAction() {
		if (PRINT_DEBUG)
			System.out.println("----redefinition");
		
		// initialize max
		List<A> bestActions = null;
		double max = -Double.MAX_VALUE;

		// get best Action from root
		for (A action : this.root.actionChildren.keySet()) {

			double Vha = this.root.actionChildren.get(action).getValue();
			if (PRINT_DEBUG) {
				System.out.println(action + "->" + Vha);
			}

			if (Vha > max) {
				// new List with this action
				max = Vha;
				bestActions = new ArrayList<>();
				bestActions.add(action);
			} else if (Vha == max) {
				// add to best actions list
				bestActions.add(action);
			}
		}

		// sample best action among bestactions
		if (PRINT_DEBUG) {
			System.out.println("list of best actions - " + bestActions);
		}
		A best = bestActions.get(RandomGen.randomInt(bestActions.size()));
		return best;
	}

}
