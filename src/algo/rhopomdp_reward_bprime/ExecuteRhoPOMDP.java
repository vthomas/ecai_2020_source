package algo.rhopomdp_reward_bprime;

import model.pomdp.Belief;

/**
 * a way to execute a rhopomdp algorithm. Encaspulates a rho-pomdp.
 *
 */
public interface ExecuteRhoPOMDP<S, A, O> {

	/**
	 * re-intialize algorithm with a specific belief;
	 * 
	 * @param b initial belief
	 */
	void initializeBelief(Belief<S> b);

	/**
	 * @return best estimated action from algorithm current internal state
	 */
	A getAction();

	/**
	 * updates algorithm internal state due to executing action a and receiving
	 * observation o
	 * 
	 * @param action      performed action
	 * @param observation received observation
	 */
	void executeAction(A action, O observation);
	
	/**
	 * get description of complementary information saved to csv file
	 */
	String getDescription();
	
	/**
	 * get information for each execution
	 */
	String getInformation();

}
