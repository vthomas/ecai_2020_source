package algo.rhopomdp_reward_bprime.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;
import tools.RandomGen;

/**
 * a tree search for rho pomdp.
 *
 */
public class RhoTree_randomEq<S, A, O> extends RhoTree<S, A, O> {

	/**
	 * create a algorithm by recursive search
	 * 
	 * @param pb   problem
	 * @param maxD maximum depth to search
	 */
	public RhoTree_randomEq(RhoPOMDP<S, A, O> pb, int maxD) {
		super(pb, maxD);
	}

	/**
	 * compute Q values from belief
	 * 
	 * @return max value
	 */
	public A computeValues(Belief<S> belief) {

		// compute all actions
		for (A action : this.problem.allAction()) {
			double actionVal = this.getActionValue(belief, this.maxDepth, action);
			this.Qval.put(action, actionVal);

			if (PRINT_VALUES)
				System.out.println(action + " -> " + actionVal);
		}

		// get max actions
		List<A> bestActions = new ArrayList<>();
		double valmax = -Double.MAX_VALUE;
		// for each action
		for (A action : this.problem.allAction()) {
			// if action is better than existing ones, new list of best actions
			if (Qval.get(action) > valmax) {
				bestActions = new ArrayList<>();
				bestActions.add(action);
				valmax = Qval.get(action);
			}
			// else if action same value as max, add action to best actions
			else if (Qval.get(action) == valmax) {
				bestActions.add(action);
			}
		}

		// return random among best actions
		int randomIndex = RandomGen.randomInt(bestActions.size());
		A bestAction = bestActions.get(randomIndex);
		
		if (PRINT_VALUES)
			System.out.println("*** actionMax -> " + bestAction+"\n");

		return bestAction;
	}

}
