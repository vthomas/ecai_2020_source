package algo.rhopomdp_reward_bprime.tree;

import algo.rhopomdp_reward_bprime.ExecuteRhoPOMDP;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.BeliefUpdate;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * create a tree search execution
 *
 * @param <S>
 * @param <A>
 * @param <O>
 */
public class ExecRhoTree<S, A, O> implements ExecuteRhoPOMDP<S, A, O> {

	/**
	 * current belief
	 */
	Belief<S> currentBelief;

	/**
	 * last action selected
	 */
	A selectAction;

	/**
	 * treeSearch algorithm
	 */
	private RhoTree<S, A, O> tree;

	/**
	 * current problem
	 */
	private RhoPOMDP<S, A, O> rhopb;

	/**
	 * create execution of rhotree algorithm
	 * 
	 * @param problem  to solve
	 * @param maxDepth search depth
	 */
	public ExecRhoTree(RhoPOMDP<S, A, O> problem, int maxDepth) {
		this.rhopb = problem;
		this.tree = new RhoTree<>(problem, maxDepth);
	}

	/**
	 * create execution of rhotree algorithm
	 * 
	 * @param problem  to solve
	 * @param maxDepth search depth
	 * @param random   if true => select action randomly among best actions
	 */
	public ExecRhoTree(RhoPOMDP<S, A, O> problem, int maxDepth, boolean random) {
		this.rhopb = problem;
		if (random)
			this.tree = new RhoTree_randomEq<S, A, O>(problem, maxDepth);
		else
			this.tree = new RhoTree<S, A, O>(problem, maxDepth);
	}

	@Override
	/**
	 * initialize current belief as initial belief
	 */
	public void initializeBelief(Belief<S> b) {
		this.currentBelief = b;
	}

	@Override
	public A getAction() {
		this.selectAction = tree.search(this.currentBelief);
		return this.selectAction;
	}

	@Override
	public void executeAction(A action, O observation) {
		// update current belief
		BeliefUpdate<S, A, O> updateB = new BeliefUpdate<S, A, O>(this.rhopb);
		this.currentBelief = updateB.updateBelief(currentBelief, action, observation);
	}

	@Override
	public String getDescription() {
		String string = "depth;A_selected;";
		for (A action : this.rhopb.allAction()) {
			string += "Q_" + action + ";";
		}
		return string;
	}

	@Override
	public String getInformation() {
		String res = this.tree.maxDepth + ";";
		res += this.selectAction + ";";
		// add values
		for (A action : this.rhopb.allAction()) {
			res += this.tree.getQval().get(action) + ";";
		}
		return res;
	}

}
