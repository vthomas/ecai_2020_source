package algo.rhopomdp_reward_bprime.tree;

import java.util.HashMap;
import java.util.Map;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * a tree search for rho pomdp.
 *
 */
public class RhoTree<S, A, O> {

	public static boolean PRINT_VALUES = false;

	/**
	 * QValues one state
	 */
	protected Map<A, Double> Qval;

	/**
	 * rhoPOMDP problem
	 */
	RhoPOMDP<S, A, O> problem;

	/**
	 * maxdepth for recursion
	 */
	int maxDepth;

	/**
	 * create a algorithm by recursive search
	 * 
	 * @param pb   problem
	 * @param maxD maximum depth to search
	 */
	public RhoTree(RhoPOMDP<S, A, O> pb, int maxD) {
		this.problem = pb;
		this.maxDepth = maxD;
		this.Qval = new HashMap<>();
	}

	/**
	 * compute Q values from belief
	 * 
	 * @return max value
	 */
	public A computeValues(Belief<S> belief) {
		// double MAX
		double MaxValue = -Double.MAX_VALUE;
		A bestAction = null;

		// compute all actions
		for (A action : this.problem.allAction()) {
			double actionVal = this.getActionValue(belief, this.maxDepth, action);
			this.Qval.put(action, actionVal);

			if (PRINT_VALUES)
				System.out.println(action + " -> " + actionVal);
			if (MaxValue < actionVal) {
				MaxValue = actionVal;
				bestAction = action;
			}
		}
		if (PRINT_VALUES)
			System.out.println("*** actionMax -> " + bestAction+"\n");

		return bestAction;
	}

	/**
	 * get value by tree search
	 * 
	 * @param b        belief
	 * @param depthMax max depth for computation
	 * @return value of belief b
	 */
	public double getValue(Belief<S> b, int depthMax) {
		// if end of recursion
		if (depthMax == 0)
			return 0;

		// max next value and best action
		double maxNextValue = -Double.MAX_VALUE;

		// assess each action
		for (A action : this.problem.allAction()) {
			// get value of action a
			double actionVal = getActionValue(b, depthMax, action);

			// action value computed
			if (actionVal > maxNextValue) {
				maxNextValue = actionVal;
			}
		}

		return maxNextValue;
	}

	/**
	 * get value from action a in belief b
	 * 
	 * @param b        belief from which value is computed
	 * @param action   considered action to estimate
	 * @param depthMax recursion depth max
	 * @return value for this action Q(b,a)
	 */
	protected double getActionValue(Belief<S> b, int depthMax, A action) {
		// estimate expectedActionValue
		double actionVal = 0;

		// for each observation
		for (O obs : this.problem.allObs()) {
			// compute probability of observation
			// P(O | a) = Sum_{s,s'} P(O|s,s',a) P(s,s'|a)
			// P(O | a) = Sum_{s,s'} P(O|s,s',a) P(s'|a,s) P(s)
			double proba = 0;

			// save arrival state probabilities
			Map<S, Double> pS = new HashMap<>();

			// for each s
			for (S s : b.keySet()) {
				// for all reachable s'
				Distribution<S> transitionFromS = this.problem.transition(s, action);
				for (S sprime : transitionFromS) {
					// p(s'|s,a)
					double probaT = transitionFromS.getProba(sprime);
					// p(o|s,s',a)
					double probaO = this.problem.observation(s, action, sprime).getProba(obs);
					// belief s
					double bs = b.get(s);

					// add proba to pSprime
					double deltaPSPrime = probaO * probaT * bs;
					if (!pS.containsKey(sprime))
						pS.put(sprime, 0.);
					pS.put(sprime, pS.get(sprime) + deltaPSPrime);
					proba += deltaPSPrime;
				}

			}

			// if observation is possible
			if (proba > 0) {
				// proba is P(O|a) (computed)
				// ps is P(s',o|a)
				// estimate immediate reward (myopic)

				// compute bPrime
				// TO CHECK
				Belief<S> bPrime = new Belief<>();
				// P(s'|o) = P(s',o|a) / P(o|a)
				for (S sPrime : pS.keySet()) {
					bPrime.put(sPrime, pS.get(sPrime) / proba);
				}

				// compute r
				double r = this.problem.reward(b, action, bPrime);

				// update actionValue
				actionVal += proba * (r + this.problem.getGamma() * this.getValue(bPrime, depthMax - 1));
			}
		}
		return actionVal;
	}

	/********** COMMON METHOD FROM RhoPOMDPAlgo **********/

	/**
	 * search best estimated action by developing tree
	 * 
	 * @param belief belief from which searching
	 * @return best estimated action
	 */
	public A search(Belief<S> belief) {
		return this.computeValues(belief);
	}

	public Map<A, Double> getQval() {
		return Qval;
	}

	public void setQval(Map<A, Double> qval) {
		Qval = qval;
	}

}
