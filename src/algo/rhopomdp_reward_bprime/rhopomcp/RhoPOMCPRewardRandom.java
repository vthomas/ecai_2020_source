package algo.rhopomdp_reward_bprime.rhopomcp;

import java.util.ArrayList;
import java.util.List;

import algo.pomdp.pomcp.Bag;
import algo.pomdp.pomcp.LogDot;
import algo.pomdp.pomcp.NodeActionPOMCP;
import algo.pomdp.pomcp.NodeBeliefPOMCP;
import algo.rhopomdp_reward_bprime.rhopomcp.beliefGen.BeliefGeneration;
import algo.rhopomdp_reward_bprime.rhopomcp.rollout.Rollout;
import algo.rhopomdp_reward_bprime.rhopomcp.valueEstimator.ValueEstimate;
import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;
import tools.RandomGen;

/**
 * Rho-POMCP algorithm (based on POMCP) with changes for belief propagation
 * <p>
 * uses Nodes from pomcp
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public class RhoPOMCPRewardRandom<S, A, O> extends RhoPOMCPReward<S, A, O> {

	/**
	 * build rhoPOMCP algorithm
	 * 
	 * @param prob      problem to solve
	 * @param nbDescent number of descents for each pomcp execution
	 * @param gen       way to generate new beliefs to add through descent
	 * @param ucbCst    UCB constant
	 * @param keep      keep subtree when executing action
	 */
	public RhoPOMCPRewardRandom(RhoPOMDP<S, A, O> prob, Belief<S> initB, BeliefGeneration<S, A, O> gen,
			Rollout<S, A, O> rollout, ValueEstimate<S, A, O> estimate, double ucbCst) {
		super(prob, initB, gen, rollout, estimate, ucbCst);
	}

	/**
	 * get best action among several identical actions
	 * 
	 * @return best action from root
	 */
	public A getBestAction() {
		if (PRINT_VALUES)
			System.out.println("----redefinition");

		// initialize max
		List<A> bestActions = null;
		double max = -Double.MAX_VALUE;

		// get best Action from root
		for (A action : this.root.actionChildren.keySet()) {

			double Vha = this.root.actionChildren.get(action).getValue();
			if (PRINT_VALUES) {
				System.out.println(action + "->" + Vha);
			}

			if (Vha > max) {
				// new List with this action
				max = Vha;
				bestActions = new ArrayList<>();
				bestActions.add(action);
			} else if (Vha == max) {
				// add to best actions list
				bestActions.add(action);
			}
		}

		// sample best action among bestactions
		if (PRINT_VALUES) {
			System.out.println("list of best actions - " + bestActions);
		}
		A best = bestActions.get(RandomGen.randomInt(bestActions.size()));
		return best;
	}
}
