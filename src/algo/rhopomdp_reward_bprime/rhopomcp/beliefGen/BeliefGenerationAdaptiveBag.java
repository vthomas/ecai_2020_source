package algo.rhopomdp_reward_bprime.rhopomcp.beliefGen;

import java.util.ArrayList;
import java.util.List;

import algo.pomdp.pomcp.Bag;
import algo.pomdp.pomcp.NodeBeliefPOMCP;
import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * generate new bag of particles by using importance sampling.
 * <li>sample state according to transition
 * <li>add weight corresponding to P(O|s)
 * 
 * this generator adapts the number of particles depending on the number of
 * visits.
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public class BeliefGenerationAdaptiveBag<S, A, O> implements BeliefGeneration<S, A, O> {

	/**
	 * rho-pomdp used for simulation/generation
	 */
	RhoPOMDP<S, A, O> prob;

	/**
	 * max number of compatible states to generate each time
	 */
	int nbMax;

	/**
	 * threshold before reducing the number of generated particles.
	 */
	private int nb_visits_0;

	/**
	 * build an adaptative beliefgeneration
	 * 
	 * @param pb         rhopomdp problem to use
	 * @param nMax       max number of states to generate
	 * @param vThreshold visit threshold
	 */
	public BeliefGenerationAdaptiveBag(RhoPOMDP<S, A, O> pb, int nMax, int vThreshold) {
		this.prob = pb;
		this.nbMax = nMax;
		this.nb_visits_0 = vThreshold;
	}

	@Override
	public Bag<S> generate(NodeBeliefPOMCP<S, A, O> h, Bag<S> previousBag, S previous, A action, O obs, S STrajectory) {
		// generated belief
		Bag<S> generatedBag = new Bag<>();

		// 1. determine number of particles to sample to haz
		int n_h = this.nbParticlesfromVisits(h.getVisit());
		NodeBeliefPOMCP<S, A, O> haz = h.actionChildren.get(action).getBeliefChildren().get(obs);
		int n_haz = this.nbParticlesfromVisits(haz.getVisit());

		// create list of initial state
		List<S> initialS = new ArrayList<S>();

		// 2. determine n_h particles from previousBag
		// belief from previous bag
		Belief<S> belief = new Belief<>(previousBag);
		for (int i = 0; i < n_h; i++) {
			initialS.add(belief.sample());
		}

		// 3. determine (n_haz-n_h) particles from beliefnode h
		// belief from previous h belief
		belief = new Belief<>(h.getBag());
		for (int i = 0; i < n_haz - n_h; i++) {
			initialS.add(belief.sample());
		}

		// sample nbWished particles
		for (S state : initialS) {

			// get distribution of sprime from state
			Distribution<S> spDist = this.prob.transition(state, action);

			// sample sprime and o from s
			S sPrime = spDist.sample();

			// get observation probability
			double pObs = this.prob.observation(state, action, sPrime).getProba(obs);

			if (pObs > 0) {
				generatedBag.addToBag(sPrime, pObs);
			}

		}

		// estimate weigth of trajectory state
		double pObsTRajectory = this.prob.observation(previous, action, STrajectory).getProba(obs);
		generatedBag.addToBag(STrajectory, pObsTRajectory);

		if (generatedBag.isEmpty())
			throw new Error("no particles " + previousBag + " : " + action + "," + obs);

		return generatedBag;
	}

	@Override
	public Bag<S> generateInitialBag(S state, Belief<S> initialBelief) {
		// create empty bag
		Bag<S> bag = new Bag<>();

		// add state s to the bag
		bag.addToBag(state, 1.0);

		// add sampled states from initialbelief (nbWished times)
		for (int i = 0; i < this.nbMax; i++) {
			S sampledS = initialBelief.sample();
			bag.addToBag(sampledS, 1);
		}

		return bag;
	}

	/**
	 * return number of particles from number of visits
	 * 
	 * @param visits number of visits of the node
	 * @return number of particles associated to beta.
	 */
	public int nbParticlesfromVisits(int visits) {
		// if visits under threshold
		if (visits <= nb_visits_0)
			return this.nbMax;
		else
			// if higher, modify number of particles to generate
			return (int) (this.nbMax * 1.0 / (visits - nb_visits_0 + 1));
	}

}
