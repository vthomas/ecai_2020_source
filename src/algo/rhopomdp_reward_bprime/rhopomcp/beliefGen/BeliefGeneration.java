package algo.rhopomdp_reward_bprime.rhopomcp.beliefGen;

import algo.pomdp.pomcp.Bag;
import algo.pomdp.pomcp.NodeBeliefPOMCP;
import model.pomdp.Belief;

/**
 * how to estimate new small bag beta_{t+1} from existing one beta_t
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public interface BeliefGeneration<S, A, O> {

	/**
	 * generate belief
	 * 
	 * @param h           beliefnode from which propagating
	 * @param previousBag previous small bag
	 * @param obs         considered observation (constraint)
	 * @param Sprime      state used during trajectory
	 * @return new small bag
	 */
	Bag<S> generate(NodeBeliefPOMCP<S, A, O> h, Bag<S> previousBag, S previous, A action, O obs, S Sprime);

	/**
	 * generate initial bag from initial belief state
	 * 
	 * @return initial bag for this trajectory
	 */
	Bag<S> generateInitialBag(S state, Belief<S> initialBelief);

}
