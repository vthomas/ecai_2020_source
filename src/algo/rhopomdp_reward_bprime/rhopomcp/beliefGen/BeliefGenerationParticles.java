package algo.rhopomdp_reward_bprime.rhopomcp.beliefGen;

import algo.pomdp.pomcp.Bag;
import algo.pomdp.pomcp.NodeBeliefPOMCP;
import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * generate new bag of particles by using importance sampling.
 * <li>sample state according to transition
 * <li>add weight corresponding to P(O|s)
 *
 * @param <S>
 * @param <A>
 * @param <O>
 */
public class BeliefGenerationParticles<S, A, O> implements BeliefGeneration<S, A, O> {

	/**
	 * rho pomdp used for simulation/generation
	 */
	RhoPOMDP<S, A, O> prob;

	/**
	 * number of compatible states to generate each time
	 */
	int nbWishedParticles;

	/**
	 * build a beliefgeneration reject
	 * 
	 * @param pb       rhopomdp problem to use
	 * @param nbWished number of compatible states to generate
	 */
	public BeliefGenerationParticles(RhoPOMDP<S, A, O> pb, int nbWished) {
		this.prob = pb;
		this.nbWishedParticles = nbWished;
	}

	@Override
	public Bag<S> generate(NodeBeliefPOMCP<S, A, O> h,Bag<S> previousBag, S previous, A action, O obs, S STrajectory) {
		// generated belief
		Bag<S> generatedBag = new Bag<>();

		// belief from initiale bag
		Belief<S> belief = new Belief<>(previousBag);

		// sample nbWished particles
		for (int i = 0; i < nbWishedParticles; i++) {
			// sample state from belief
			S state = belief.sample();

			// get distribution of sprime from state
			Distribution<S> spDist = this.prob.transition(state, action);

			// sample sprime and o from s
			S sPrime = spDist.sample();

			// get observation probability
			double pObs = this.prob.observation(state, action, sPrime).getProba(obs);

			if (pObs > 0) {
				generatedBag.addToBag(sPrime, pObs);
			}

		}

		// estimate weigth of trajectory state
		double pObsTRajectory = this.prob.observation(previous, action, STrajectory).getProba(obs);
		generatedBag.addToBag(STrajectory, pObsTRajectory);

		if (generatedBag.isEmpty())
			throw new Error("no particles " + previousBag + " : " + action + "," + obs);

		return generatedBag;
	}

	@Override
	public Bag<S> generateInitialBag(S state, Belief<S> initialBelief) {
		// create empty bag
		Bag<S> bag = new Bag<>();

		// add state s to the bag
		bag.addToBag(state, 1.0);

		// add sampled states from initialbelief (nbWished times)
		for (int i = 0; i < this.nbWishedParticles; i++) {
			S sampledS = initialBelief.sample();
			bag.addToBag(sampledS, 1);
		}

		return bag;
	}

}
