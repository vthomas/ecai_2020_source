package algo.rhopomdp_reward_bprime.rhopomcp;

import algo.pomdp.pomcp.LogDot;
import algo.pomdp.pomcp.NodeActionPOMCP;
import algo.pomdp.pomcp.NodeBeliefPOMCP;
import algo.rhopomdp_reward_bprime.ExecuteRhoPOMDP;
import algo.rhopomdp_reward_bprime.rhopomcp.beliefGen.BeliefGeneration;
import algo.rhopomdp_reward_bprime.rhopomcp.rollout.Rollout;
import algo.rhopomdp_reward_bprime.rhopomcp.valueEstimator.ValueEstimate;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * encpasulate POMCP algorithm
 *
 * @param <S>
 * @param <A>
 * @param <O>
 */
public class ExecuteRhoPOMCP<S, A, O> implements ExecuteRhoPOMDP<S, A, O> {

	/**
	 * algorithm used
	 */
	private RhoPOMCPReward<S, A, O> rhoPomcp;

	/**
	 * problem to solve
	 */
	private RhoPOMDP<S, A, O> prob;

	/**
	 * number of descents by iteration
	 */
	private int nbDescent;

	/**
	 * how to build belief at each time step
	 */
	BeliefGeneration<S, A, O> beliefGenerator;

	/**
	 * rollout
	 */
	Rollout<S, A, O> rollout;

	/**
	 * UCB constant
	 */
	double ucbCst;

	/**
	 * keep the subtree after execution
	 */
	boolean subtreeKept = true;

	/**
	 * way to estimate values during backpropagation
	 */
	private ValueEstimate<S, A, O> estimate = null;

	/**
	 * creates rho-pomcp algorithm to be executed
	 * 
	 * @param pb        problem to solve
	 * @param beliefGen object to generate beliefs while going through belief tree
	 * @param estimate  the way value is estimated during backpropagation
	 * @param descents  number of descents by each rho pomcp execution
	 * @param ucbCst    ucb constant
	 * @param keep      keep subtree when executing action
	 */
	public ExecuteRhoPOMCP(RhoPOMDP<S, A, O> pb, BeliefGeneration<S, A, O> beliefGen, Rollout<S, A, O> rollout,
			ValueEstimate<S, A, O> estimate, int descents, double ucb, boolean keep) {
		this.prob = pb;
		this.ucbCst = ucb;
		this.beliefGenerator = beliefGen;
		this.estimate = estimate;
		this.rollout = rollout;
		this.nbDescent = descents;
		this.subtreeKept = keep;
	}

	@Override
	/**
	 * initializes a new rhoPOMCP algorithm
	 */
	public void initializeBelief(Belief<S> b0) {
		// creates a new rhopomcp algo (and creates root node)
		this.rhoPomcp = new RhoPOMCPReward<>(prob, b0, beliefGenerator, rollout, estimate, ucbCst);
	}

	@Override
	public A getAction() {
		// launch search for root (assumed to be correct)
		return rhoPomcp.search(nbDescent);
	}

	@Override
	public void executeAction(A action, O observation) {

		this.rhoPomcp.modifyBeliefExecution(action, observation, this.subtreeKept);
	}

	@Override
	public String getDescription() {
		String desc = "nbDescents;";
		for (A a : this.prob.allAction())
			desc += a + ";";
		return desc;
	}

	@Override
	public String getInformation() {
		NodeBeliefPOMCP<S, A, O> root = this.rhoPomcp.root;
		String info = root.getVisit() + ";";
		for (A a : this.prob.allAction()) {
			NodeActionPOMCP<S, A, O> nodeActionPOMCP = root.actionChildren.get(a);
			info += nodeActionPOMCP.getNbVisits() + ";";
		}
		return info;
	}

}
