package algo.rhopomdp_reward_bprime.rhopomcp;

import algo.rhopomdp_reward_bprime.ExecuteRhoPOMDP;
import algo.rhopomdp_reward_bprime.rhopomcp.beliefGen.BeliefGeneration;
import algo.rhopomdp_reward_bprime.rhopomcp.rollout.Rollout;
import algo.rhopomdp_reward_bprime.rhopomcp.valueEstimator.ValueEstimate;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * encpasulate POMCP algorithm
 *
 * @param <S>
 * @param <A>
 * @param <O>
 */
public class ExecuteRhoPOMCPTime<S, A, O> implements ExecuteRhoPOMDP<S, A, O> {

	/**
	 * algorithm used
	 */
	private RhoPOMCPAnytime<S, A, O> rhoPomcp;

	/**
	 * problem to solve
	 */
	private RhoPOMDP<S, A, O> prob;

	/**
	 * timeBudget for each iteration
	 */
	private long timeBudget;

	/**
	 * how to build belief at each time step
	 */
	BeliefGeneration<S, A, O> beliefGenerator;

	/**
	 * rollout used
	 */
	Rollout<S, A, O> rollout;

	/**
	 * UCB constant
	 */
	double ucbCst;

	/**
	 * keep the subtree after execution
	 */
	boolean subtreeKept = true;

	/**
	 * way value is estimated during backpropagation
	 */
	private ValueEstimate<S, A, O> estimate;

	/**
	 * creates rho-pomcp algorithm to be executed
	 * 
	 * @param pb        problem to solve
	 * @param beliefGen object to generate beliefs while going through belief tree
	 * @param rollout   rollout
	 * @param estimate  how to estimate node value
	 * @param timeB     available time budget
	 * @param ucb       ucb constant
	 * @param keep      keep subtree when executing action
	 */
	public ExecuteRhoPOMCPTime(RhoPOMDP<S, A, O> pb, BeliefGeneration<S, A, O> beliefGen, Rollout<S, A, O> rollout,
			ValueEstimate<S, A, O> estimate, long timeB, double ucb, boolean keep) {
		this.prob = pb;
		this.ucbCst = ucb;
		this.beliefGenerator = beliefGen;
		this.estimate = estimate;
		this.rollout = rollout;
		this.timeBudget = timeB;
		this.subtreeKept = keep;
	}

	@Override
	/**
	 * initializes a new rhoPOMCP algorithm
	 */
	public void initializeBelief(Belief<S> b0) {
		// creates a new rhopomcp algo (and creates root node)
		this.rhoPomcp = new RhoPOMCPAnytime<>(prob, b0, beliefGenerator, rollout, estimate, ucbCst);
	}

	@Override
	public A getAction() {
		// launch search for root (assumed to be correct)
		return rhoPomcp.search(this.timeBudget);
	}

	@Override
	public void executeAction(A action, O observation) {
		this.rhoPomcp.modifyBeliefExecution(action, observation, this.subtreeKept);
	}

	@Override
	public String getDescription() {
		return "nbDescents;";
	}

	@Override
	public String getInformation() {
		return this.rhoPomcp.root.getVisit() + ";";
	}

}
