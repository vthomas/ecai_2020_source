package algo.rhopomdp_reward_bprime.rhopomcp;

import algo.pomdp.pomcp.LogDot;
import algo.pomdp.pomcp.LogJson;
import algo.rhopomdp_reward_bprime.rhopomcp.beliefGen.BeliefGeneration;
import algo.rhopomdp_reward_bprime.rhopomcp.rollout.Rollout;
import algo.rhopomdp_reward_bprime.rhopomcp.valueEstimator.ValueEstimate;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * rhopomcp algorithm with anytime approach
 *
 */
public class RhoPOMCPAnytime<S, A, O> extends RhoPOMCPReward<S, A, O> {

	public RhoPOMCPAnytime(RhoPOMDP<S, A, O> prob, Belief<S> initB, BeliefGeneration<S, A, O> gen,
			Rollout<S, A, O> rollout, ValueEstimate<S, A, O> estimate, double ucbCst) {
		super(prob, initB, gen, rollout, estimate, ucbCst);
	}

	/**
	 * launch search while remaining time
	 * 
	 * @param timeBudget
	 */
	public A search(long timeBudget) {
		// create a belief from root particles
		Belief<S> rootBelief = new Belief<>(root.getBag());

		// initial tree
		if (PRINT_DEBUG_TREE) {
			LogDot<S, A, O> log = new LogDot<>(root);
			System.out.println(log.toDot(3));
		}

		// number of iterations
		int it = 0;

		long startTime = System.currentTimeMillis();
		while (System.currentTimeMillis() - startTime < timeBudget) {
			if (PRINT_IT) {
				System.out.println("***** iteration : " + (it++));
			}

			this.oneDescent(rootBelief);

			if (PRINT_DEBUG_TREE) {
				LogDot<S, A, O> log = new LogDot<>(root);
				System.out.println(log.toDot(3));
			}
		}

		return this.getBestAction();
	}

}
