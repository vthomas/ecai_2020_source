package algo.rhopomdp_reward_bprime.rhopomcp.rollout;

import algo.pomdp.pomcp.Bag;
import algo.rhopomdp_reward_bprime.myopic.MyopicSearch;
import algo.rhopomdp_reward_bprime.rhopomcp.RhoPOMCPReward;
import algo.rhopomdp_reward_bprime.rhopomcp.beliefGen.BeliefGeneration;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * perform myopic rollout - use a myopic algorithm at each rollout time step.
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public class RolloutMyopic<S, A, O> implements Rollout<S, A, O> {

	/**
	 * myopic algorithm used for rollout
	 */
	MyopicSearch<S, A, O> myopic;

	/**
	 * generator of belief needed to estimate reward
	 */
	BeliefGeneration<S, A, O> beliefGen;

	/**
	 * creation of myopic rollout
	 * 
	 * @param pb problem to solve
	 */
	public RolloutMyopic(RhoPOMDP<S, A, O> pb, BeliefGeneration<S, A, O> belGen) {
		this.myopic = new MyopicSearch<>(pb);
		this.beliefGen = belGen;
	}

	@Override
	public double rollout(Bag<S> bag, int depth, RhoPOMDP<S, A, O> pb) {
		// if enough depth
		if (Math.pow(pb.getGamma(), depth) < RhoPOMCPReward.EPSILON) {
			return 0;
		}

		// get action selected by myopic algorithm
		Belief<S> initialB = new Belief<>(bag);
		A action = this.myopic.search(initialB);

		// execute transition
		S initialS = initialB.sample();
		// sample Sprime and observation
		S Sprime = pb.transition(initialS, action).sample();
		O obs = pb.observation(initialS, action, Sprime).sample();

		// generate SprimeBag from action and obs
		Bag<S> newBag = this.beliefGen.generate(null, bag, initialS, action, obs, Sprime);
		Belief<S> after = new Belief<S>(newBag);

		// estimate reward
		double reward = pb.reward(initialB, action, after);
		return reward + pb.getGamma() * this.rollout(newBag, depth + 1, pb);

	}

}
