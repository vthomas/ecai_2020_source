package algo.rhopomdp_reward_bprime.rhopomcp.rollout;

import java.util.List;

import algo.pomdp.pomcp.Bag;
import algo.rhopomdp_reward_bprime.rhopomcp.RhoPOMCPReward;
import algo.rhopomdp_reward_bprime.rhopomcp.beliefGen.BeliefGeneration;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;
import tools.RandomGen;

/**
 * Random rollout for rho-POMCP.
 * <p>
 * actions are randomly selected, beliefs need to be updated to estimate
 * rewards.
 * 
 * @author vthomas
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public class RolloutRandom<S, A, O> implements Rollout<S, A, O> {

	/**
	 * generator of belief used for estimating rewards
	 */
	BeliefGeneration<S, A, O> beliefGen;

	/**
	 * creates random rollout
	 * 
	 * @param belGen generator of beliefs used by rollout
	 */
	public RolloutRandom(BeliefGeneration<S, A, O> belGen) {
		this.beliefGen = belGen;
	}

	@Override
	public double rollout(Bag<S> bag, int depth, RhoPOMDP<S, A, O> pb) {
		if (Math.pow(pb.getGamma(), depth) < RhoPOMCPReward.EPSILON) {
			return 0;
		}

		// sample transition
		Belief<S> initialB = new Belief<S>(bag);
		S initialS = initialB.sample();
		// sample action
		List<A> actions = pb.allAction();
		A action = actions.get(RandomGen.randomInt(actions.size()));
		// sample Sprime and observation
		S Sprime = pb.transition(initialS, action).sample();
		O obs = pb.observation(initialS, action, Sprime).sample();

		// generate SprimeBag from action and obs
		Bag<S> newBag = this.beliefGen.generate(null, bag, initialS, action, obs, Sprime);
		Belief<S> after = new Belief<S>(newBag);

		// estimate reward
		double reward = pb.reward(initialB, action, after);
		return reward + pb.getGamma() * this.rollout(newBag, depth + 1, pb);
	}

}
