package algo.rhopomdp_reward_bprime.rhopomcp.valueEstimator;

import algo.pomdp.pomcp.NodeActionPOMCP;
import algo.pomdp.pomcp.NodeBeliefPOMCP;

/**
 * modify the way the new value of an action node is estimated
 * <p>
 * corresponds to the update of V(ha) in rho-POMCP algorithm
 *
 */
public interface ValueEstimate<S, A, O> {

	/**
	 * initialize new belief node
	 * 
	 * @param node    belief node to initialize
	 * @param rollout initial value
	 */
	public void initializeNode(NodeBeliefPOMCP<S, A, O> node, double rollout);

	/**
	 * perform an update of current node after action and observation
	 * 
	 * @param node     node to update value
	 * @param action   selected action
	 * @param obs      received observation
	 * @param reward   received reward
	 * @param gamma    discount factor
	 * @param simulate result of simulation from child node
	 * @return value to return to parent node
	 */
	public double update(NodeBeliefPOMCP<S, A, O> h, A action, O obs, double reward, double gamma, double simulate);

}
