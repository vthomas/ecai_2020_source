package algo.pomdp.generic;

import model.pomdp.Belief;

/**
 * interface for representing a POMDP policy base don a specific algorithm
 *
 */
public interface AlgoPOMDP<S, A, O> {

	/**
	 * initialize POMDP algorithm
	 */
	void initialize(Belief<S> b0);

	/**
	 * @return select action based on current attributes
	 */
	A getAction();

	/**
	 * @return belief actuel
	 */
	Belief<S> getBelief();

	/**
	 * perform the execution of action a and receives observation o
	 */
	void executeAction(A a, O o);

}
