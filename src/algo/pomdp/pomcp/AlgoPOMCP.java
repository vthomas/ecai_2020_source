package algo.pomdp.pomcp;

import algo.pomdp.generic.AlgoPOMDP;
import algo.pomdp.pomcp.rollout.Rollout;
import model.pomdp.Belief;
import model.pomdp.POMDP;

/**
 * algo to encapsulate POMCP approach
 * 
 * @author vthomas
 *
 * @param <S>
 * @param <A>
 * @param <O>
 */
public class AlgoPOMCP<S, A, O> implements AlgoPOMDP<S, A, O> {

	/**
	 * the POMCP class used for behaviour
	 */
	public POMCP<S, A, O> pomcp;

	/**
	 * problem to solve
	 */
	POMDP<S, A, O> problem;

	/**
	 * rollout to use
	 */
	Rollout<S, A, O> rollout;

	/**
	 * ucb constant
	 */
	double ucbCst;

	/**
	 * number of descents for each action
	 */
	int nbDescents;

	/**
	 * boolean to determine if algo keeps subtree when executing action
	 */
	boolean subtreeKept;

	/**
	 * create a POMCP algorithm
	 */
	public AlgoPOMCP(POMDP<S, A, O> pb, Rollout<S, A, O> roll, double UCB, int nbIt, boolean keep) {
		this.problem = pb;
		this.rollout = roll;
		this.ucbCst = UCB;
		this.nbDescents = nbIt;
		this.subtreeKept = keep;
	}

	@Override
	public void initialize(Belief<S> b0) {
		// create pomcp algorithm
		this.pomcp = new POMCP<S, A, O>(this.problem, b0, this.rollout, this.ucbCst);
	}

	@Override
	public A getAction() {
		// use POMCP descent for making decision
		A action = this.pomcp.search(this.nbDescents);
		return action;
	}

	@Override
	public void executeAction(A a, O o) {
		// change root of the tree and current belief
		this.pomcp.modifyBeliefExecution(a, o, this.subtreeKept);
	}

	@Override
	public Belief<S> getBelief() {
		return new Belief<>(this.pomcp.root.getBag());
	}

}
