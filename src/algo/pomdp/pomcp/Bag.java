package algo.pomdp.pomcp;

import java.util.*;

/**
 * bag of particles used during a rhoPOMCP trajectory
 * 
 * @author vthomas
 *
 */
public class Bag<S> {

	/**
	 * Map to stock cumulated particles weight
	 */
	Map<S, Double> particles;

	/**
	 * create a bag
	 */
	public Bag() {
		this.particles = new HashMap<S, Double>();
	}

	/**
	 * add a value to the bag
	 */
	public void addToBag(S state, double val) {
		double oldVal = 0;
		if (this.particles.containsKey(state)) {
			oldVal = this.particles.get(state);
		}
		this.particles.put(state, oldVal + val);

	}

	/**
	 * keyset for iterating on bag
	 * 
	 * @return
	 */
	public Set<S> keySet() {
		return this.particles.keySet();
	}

	/**
	 * return value associated to a particle
	 * 
	 * @param state
	 * @return
	 */
	public double get(S state) {
		return this.particles.get(state);
	}

	public boolean isEmpty() {
		return this.particles.isEmpty();
	}

	public String toString() {
		String res = "[";
		for (S st : this.particles.keySet()) {
			res += st + ":" + this.particles.get(st) + ",";
		}
		res += "]";
		return res;

	}

	/**
	 * normalize the bag
	 */
	public void normalize() {
		// compute sum
		double sum = 0;
		for (S s : this.particles.keySet()) {
			sum = sum + this.particles.get(s);
		}

		// normalize
		for (S s : this.particles.keySet()) {
			this.particles.put(s, this.particles.get(s) / sum);
		}
	}

}
