package algo.pomdp.pomcp.rollout;

import model.pomdp.POMDP;

/**
 * assess rollout through multiple random policy
 * 
 * @author vthomas
 *
 * @param <S>
 * @param <A>
 * @param <O>
 */
public class RolloutMultiRandom<S, A, O> implements Rollout<S, A, O> {

	/**
	 * in order to show rollout value
	 */
	private static final boolean SHOW = false;

	/**
	 * number of rollout
	 */
	int nb;

	/**
	 * create Multiple Rollout
	 * 
	 * @param nb number of rollouts
	 */
	public RolloutMultiRandom(int nb) {
		this.nb = nb;
	}

	@Override
	public double rollout(S state, int depth, POMDP<S, A, O> pomdp) {
		RolloutRandom<S, A, O> roll = new RolloutRandom<>(pomdp.getGamma());
		double sum = 0;
		for (int i = 0; i < nb; i++) {
			sum += roll.rollout(state, depth, pomdp);
		}
		double averageRoll = sum / nb;

		if (SHOW)
			System.out.println(averageRoll);

		return averageRoll;
	}

}
