package algo.pomdp.pomcp.rollout;

import algo.pomdp.pomcp.POMCP;
import model.pomdp.POMDP;

/**
 * gives always the same rollout value
 * 
 * @author vthomas
 *
 */
public class RolloutConstant<S, A, O> implements Rollout<S, A, O> {

	/**
	 * rollout value
	 */
	private double RolloutValue;
	
	/**
	 * gamma factor
	 */
	private double gamma;

	/**
	 * create a rollout constant value
	 * 
	 * @param val
	 */
	public RolloutConstant(double val, double g) {
		this.RolloutValue = val;
		this.gamma=g;
	}

	@Override
	public double rollout(S state, int depth, POMDP<S, A, O>  pomdp) {
		// if enough, stop recursion
		if (Math.pow(this.gamma, depth) < POMCP.EPSILON)
			return 0;

		// no recursive rollout
		return RolloutValue;
	}

}
