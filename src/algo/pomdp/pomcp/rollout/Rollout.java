package algo.pomdp.pomcp.rollout;

import model.pomdp.POMDP;

/**
 * interface for rollout
 * 
 * @author vthomas
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public interface Rollout<S, A, O> {

	/**
	 * recursive rollout from state state
	 * 
	 * @param state state from which making a rollout
	 * @param depth desired depth
	 * @return value of rollout
	 */
	double rollout(S state, int depth, POMDP<S, A, O> pomdp);

}
