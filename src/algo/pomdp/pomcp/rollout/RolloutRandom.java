package algo.pomdp.pomcp.rollout;

import java.util.List;

import algo.pomdp.pomcp.POMCP;
import model.pomdp.POMDP;
import tools.RandomGen;

/**
 * assess rollout through random policy
 * 
 * @author vthomas
 *
 * @param <S>
 * @param <A>
 * @param <O>
 */
public class RolloutRandom<S, A, O> implements Rollout<S, A, O> {

	/**
	 * gamma factor
	 */
	double gamma;

	/**
	 * create rollout
	 * 
	 * @param g gamma factor
	 */
	public RolloutRandom(double g) {
		this.gamma = g;
	}

	@Override
	public double rollout(S state, int depth, POMDP<S, A, O> pomdp) {
		// if enough, stop recursion
		if (Math.pow(this.gamma, depth) < POMCP.EPSILON)
			return 0;

		// random action
		List<A> actions = pomdp.allAction();
		int rand = RandomGen.randomInt(actions.size());
		A action = actions.get(rand);

		// sampling sPrime
		S sPrime = pomdp.transition(state, action).sample();

		// recursive call
		double reward = pomdp.r(state, action);
		double R = reward + this.gamma * this.rollout(sPrime, depth + 1, pomdp);

		return R;
	}

}
